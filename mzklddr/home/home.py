from flask import Blueprint, render_template, abort, url_for, current_app
from mzklddr.models.post import Post
import datetime
import random
from flask_babel import _
from mzklddr.helpers import util as u
from mzklddr.helpers import collect
from mzklddr.helpers import sql
from mzklddr.helpers.caching import cache


bp = Blueprint('home', __name__)

@bp.route('/')
@cache.cached()
def home():
    data = {}
    day = datetime.date.today()
    start_date = day.strftime('%Y-%m-%d')
    data['big_cities'] = sql.get_cities_by_ids(
        [1,8,5,1412801590,1413406572,4,7,15,6,17,100,1439757759])
    random.shuffle(data['big_cities'])
    post_list = collect.post_list() 
    data['posts'] = post_list
    data['events'] = u.dedup_events(sql.get_events(date_start=start_date,
       date_range_end=start_date, rows=99999))
    data['groups'] = sql.get_city_groupings()
    data['cities'] = sql.get_cities()
 

    return render_template('home.html', data=data)
