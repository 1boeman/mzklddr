from flask import Blueprint, render_template, current_app, redirect, abort, url_for
from flask_babel import format_datetime, _
from mzklddr.helpers import sql
from mzklddr.helpers import util as u
from mzklddr.helpers import collect
from mzklddr.models.venue import Venue
from mzklddr.helpers import collect
from mzklddr.helpers.caching import cache


bp=Blueprint('locaties', __name__)


@bp.route('/<venue_id>')
@cache.cached()
def venue(venue_id):

    crumbs = u.crumb(url_for('uitgaan.city'), _('Locations'))

    logger = current_app.logger
    data = {}
    try:
        venue = Venue(venue_id)
    except Exception as e:
        # try to REDIRECT LEGACY URLS
        if len(venue_id) > len(get_venue_id_from_url_param(venue_id)):
            if sql.get_venue(get_venue_id_from_url_param(venue_id)):
                return redirect(collect.venue_link(get_venue_id_from_url_param(venue_id)),code=303)
        abort(404) 
    venue_addresses = venue.get_venue_addresses()
    u.crumb(collect.city_link(
            venue_addresses[0]["city_id"],
            venue_addresses[0]['city_name']),
        venue_addresses[0]['city_name'], crumbs)
    data['venue'] = venue

    u.crumb(collect.venue_link(venue.venue_id), venue.title, crumbs)
    events = sql.get_events(venue_id = venue_id) 
    data['events'] = u.dedup_events(events)
    return render_template(
        'locaties_venue.html',
        data=data, breadcrumbs=crumbs)


def get_venue_id_from_url_param(venue_id_city):
    logger = current_app.logger
    arr = venue_id_city.split("-")
    venue_id = ""
    if len(arr) > 2:
        #multiple dashes either in city_name or in venue_id
        cities = sql.get_cities_with_names_containing_dash()
        for c in cities:
            if c['city_name'] in venue_id_city:
                venue_id = venue_id_city.replace('-' + c["city_name"],"")
                current_app.logger.info("found city_name with dashes")
                current_app.logger.info(c["city_name"])
        if not len(venue_id):
            # multiple dashes not in city_name so may be in  venue_name
            venue_id_with_dashes = "-".join(arr)
            venue = sql.get_venue(venue_id_with_dashes) 
            if not venue:
                arr.pop()
            venue_id = "-".join(arr)
            current_app.logger.info("dashes in venue_name")
    else:
        return arr[0]
    return venue_id
