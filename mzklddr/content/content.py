from flask import Blueprint, render_template, abort, url_for
from mzklddr.models.post import Post
from flask_babel import _
from mzklddr.helpers import util as u


bp = Blueprint('content', __name__)


@bp.route('/privacy-verklaring')
def privacy_policy():
    crumbs = u.crumb(url_for('content.privacy_policy'), _('Privacy Policy'))
    data = {}
    return render_template('privacy_policy.html',
        data=data, breadcrumbs=crumbs )
