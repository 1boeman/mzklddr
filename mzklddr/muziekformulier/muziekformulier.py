import os
import json
import logging
import datetime
import click
from flask import Blueprint, render_template, g, abort, current_app, session, url_for
from flask_login import current_user
from flask_babel import format_datetime, _, lazy_gettext
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from werkzeug.utils import secure_filename
from wtforms import StringField, SelectField, DateField, BooleanField, TextAreaField, HiddenField
from wtforms.validators import DataRequired, InputRequired, ValidationError, Optional, URL
from mzklddr.helpers import sql, collect, util as u
from mzklddr.models.post import Post, Form_data_container


bp=Blueprint('muziekformulier', __name__)


class MForm(FlaskForm):
    eventType = SelectField(lazy_gettext('Type of event'), 
                            choices=[
                                (0,lazy_gettext(' - Select - ')),
                                ('concert',lazy_gettext("Concert or performance")),
                                ("festival",lazy_gettext("Festival or party")),
                                ("other",lazy_gettext("Something else"))
                            ], 
                            validators=[InputRequired()])
    eventDates = StringField(lazy_gettext('Date(s) of event'), validators=[InputRequired(lazy_gettext('Please provide one or more dates for the event'))])
    checkboxCity = BooleanField(lazy_gettext('City not in dropdown list? Select this!'))
    checkboxVenue = BooleanField(lazy_gettext('Venue/location not in dropdown list? Select this!'))
    title = StringField(lazy_gettext('Title of event'), validators=[InputRequired(lazy_gettext('Please provide a title for the event'))])
    link = StringField(lazy_gettext('Link to event'), default="https://", validators=[InputRequired(_('Please provide a working link / url for the event')), URL(message=_('Please provide a working link to event or artist information'))])
    cityAsText = TextAreaField(lazy_gettext('Please specify the name of the city, municipality or village hosting the event or performance'))
    venueAsText = TextAreaField(lazy_gettext('Please specify the name and address of the venue, club or building where the event will be hosted'))
    descText = TextAreaField(lazy_gettext('Remarks and/or extra information (optional)'))
    submitThis = HiddenField()
    media = HiddenField()
    post_id=HiddenField()


class AForm(FlaskForm):
    title = StringField(lazy_gettext('Name of artist/group'), validators=[InputRequired(lazy_gettext('Please provide the name of the artist'))])
    link = StringField(lazy_gettext('Link to website or main social media account'), default="https://" ,validators=[InputRequired(lazy_gettext('Please provide a working link/ url')),URL(message=lazy_gettext('Please provide a working link to the artist\'s website or social media account'))])
    link2 = StringField(lazy_gettext('Link to video or audio samples'),default="https://", validators=[InputRequired(lazy_gettext('Please provide a working link/ url')), URL(message=lazy_gettext('Please provide a working link to a website or social media containing video or audio examples'))])
    descText = TextAreaField(lazy_gettext('More information about the artist/group'),validators=[InputRequired(lazy_gettext('Please provide a descriptive text about the artist/group'))])
    media = HiddenField()
    post_id=HiddenField()


def FileSizeLimit(max_size_in_mb):
    max_bytes = max_size_in_mb*1024*1024
    def file_length_check(form, field):
        if len(field.data.read()) > max_bytes:
            raise ValidationError(f"File size must be less than {max_size_in_mb}MB")
        field.data.seek(0)
    return file_length_check


def ExtensionCheck(form, field):
    allowed_extensions =  {'bmp', 'png', 'jpg', 'jpeg', 'gif'}
    filename_list = field.data.filename.split('.')
    if len(filename_list) < 2 or filename_list[-1].lower() not in allowed_extensions:
        raise ValidationError(f"File must be image of one of following types: {', '.join(allowed_extensions)}")


class PhotoForm(FlaskForm):
    photo = FileField(lazy_gettext('Image - poster, flyer or logo  - jpg, gif or png (optional)'),validators=[FileRequired(), FileSizeLimit(max_size_in_mb=8), ExtensionCheck])
    uniqid = HiddenField()


def addVenuesToForm(city_id):
    if city_id != "":
        venues = sql.get_city_venues(city_id)
        city = sql.get_city(city_id)
        venue_choices = [(str(v['address_id']) + '_' + str(v['venue_id']), v['venue_address_title']) for v in venues]
        venue_choices = [('',lazy_gettext('2.  select a venue in ' + city['city_name'] + ' *'))] + venue_choices
        setattr(MForm,'venueSelect',SelectField(lazy_gettext('Venue'), choices=venue_choices, validators=[Optional()]))
    else:
        removeVenuesFromForm()


def post_ownership_check(post):
    if not current_user.is_authenticated or \
            current_user.user_id != post.user_id:
        abort(403)


def removeVenuesFromForm():
    if hasattr(MForm,'venueSelect'):
        delattr(MForm,'venueSelect')


def addCityToForm():
    cities = sql.get_cities()
    city_choices = [(c['city_id'],c['city_name'] + ' (' + collect.country_code (c['country_name']) + ')' ) for c in cities]
    city_choices = [('',lazy_gettext('1. select a place * '))] + city_choices
    setattr(MForm,'citySelect',SelectField(lazy_gettext('Place name'), choices=city_choices))


@bp.route('/xhrsubmit_artistform',methods=['POST'])
def xhr_artist():
    logger = current_app.logger
    form = AForm()
    post = None
    tpl_data={}
    if not current_user.is_authenticated:
        abort(403)

    if len(form.post_id.data):
        post = Post(form.post_id.data)
        post_ownership_check(post)
        logger.debug(f"user {current_user.user_id} opens post {post.post_id} for editing" )

    if form.validate_on_submit():
        result_data = Form_data_container()
        form.populate_obj(result_data)
        post_data = result_data.toJSON()

        if not post:
            post = Post()

        post.save_user_post(
            user_id=current_user.user_id,
            post_title=form.title.data,
            post_data=result_data.toJSON(),
            post_type='artist',
            published=0)
        save_media(form, post)
        form = AForm(formdata=None)
        tpl_data['posts'] = collect.post_list(
            user_id=current_user.user_id,
            post_ids=[post.post_id])

        return render_template('muziekformulier_success.html',data=tpl_data)

    return render_template('muziekformulier_artist_include.html', form=form, data=tpl_data)


@bp.route('/xhrsubmit',methods=['POST'])
def xhr():
    if not current_user.is_authenticated:
        abort(403)

    tpl_data={}
    logger = current_app.logger
    city_error = _('Please specify a place name')
    venue_error = _('Please specify a venue')
    addCityToForm()
    data_form = MForm()
    addVenuesToForm(data_form.citySelect.data)
    post=None
    form = MForm()

    # check ownership 
    if len(form.post_id.data):
        post = Post(form.post_id.data)
        post_ownership_check(post)
        logger.debug(f"user {current_user.user_id} opens post {post.post_id} for editing" )

    if form.submitThis.data == 'y' and form.validate():
        validated = True
        checkboxCity = form.checkboxCity.data
        checkboxVenue = form.checkboxVenue.data
        cityAsText = form.cityAsText.data
        venueAsText = form.venueAsText.data
        citySelect = form.citySelect.data

        # if venue has not yet appeared in the form 
        #  -( e.g., we are still selecting /  a city), 
        # we have to unset venuSelect 
        # because otherwise form.venueSelect.data will persist between submits
        if "venueSelect" in form:
            venueSelect = form.venueSelect.data
            checkboxVenue = form.checkboxVenue.data
        else:
            venueSelect = None
            checkboxVenue = None

        # do the logic:
        # if checkboxcity - both cityastext and venueastext are needed
        if checkboxCity:
            if venueAsText.strip() == "" or cityAsText.strip() == "":
                form.checkboxCity.errors.append(city_error)
                validated = False

                form.checkboxCity.errors.append(venue_error)
                validated = False
        # if checkboxvenue - venueastext is needed
        elif checkboxVenue:
            if venueAsText.strip() == "":
                form.checkboxVenue.errors.append(venue_error)
                validated = False
        # no checkboxes-  citySelect and venueSelect are both needed
        else:
            if not len(citySelect):
               form.checkboxVenue.errors.append(city_error)
               validated = False
            elif not venueSelect or not len(venueSelect):
                form.checkboxVenue.errors.append(venue_error)
                validated = False

        if validated:
            #write to db
            result_data = Form_data_container()
            form.populate_obj(result_data)

            if checkboxCity:
                result_data.citySelect=None
                result_data.venueSelect=None
            else:
                #result_data['citySelect'] = form.citySelect.data
                result_data.cityAsText = None
                if checkboxVenue:
                    #result_data['venueAsText'] = venueAsText
                    result_data.venueSelect=None
                else:
                    #result_data['venueSelect'] = venueSelect,
                    result_data.venueAsText = None
            if not post:
                post = Post()

            post.save_user_post(
                user_id=current_user.user_id,
                post_title=form.title.data,
                post_data=result_data.toJSON(),
                post_type='event',
                published=0)

            save_media(form, post)

            # reset form
            form = MForm(formdata=None)
            tpl_data['posts'] = collect.post_list(user_id=current_user.user_id)
            return render_template('muziekformulier_success.html',data=tpl_data)
    return render_template('muziekformulier_include.html', form=form, data=tpl_data)


def save_media(form, post):
    logger=current_app.logger
    logger.debug('muziekformulier save_media ' + str(form.media.data) )
    if form.media.data:
        post.save_single_media(form.media.data)
    elif form.post_id.data:
        post.delete_media() 


# update
@bp.route('/edit/<post_id>', methods=['GET'])
def edit_post(post_id):
    post = Post(post_id)
    post_ownership_check(post)
    post_obj = post.form_object
    tpl_data = {"edit":True, "post_id":post.post_id}
    if post.post_type == 'artist':
        form = AForm(obj=post_obj)
        form.post_id.data=post_id
        tpl = 'muziekformulier-artist.html'
    else:    
        addCityToForm()
        if post_obj.citySelect:
            addVenuesToForm(post_obj.citySelect)
        
        form = MForm(obj=post_obj)
        form.submitThis.data=""
        form.post_id.data=post_id
        tpl = 'muziekformulier.html'
    return render_template(tpl, data=tpl_data, form=form)


@bp.route('/delete/<post_id>', methods=['POST'])
def delete_post(post_id):
    post = Post(post_id)
    post_ownership_check(post)
    result = post.delete()
    return {"result" : result}


@bp.route('/artist',methods=['GET'])
def artistform():
    crumbs = u.crumb(url_for('tips.listing', page=0), _('Tips'))
    form = AForm()
    tpl_data = {}
    return render_template(
        'muziekformulier-artist.html',
        data=tpl_data,
        form=form,
        breadcrumbs=crumbs)


@bp.route('/', methods=['GET'])
def formulier():
    crumbs = u.crumb(url_for('tips.listing', page=0), _('Tips'))
    u.crumb(url_for('muziekformulier.formulier'), _('Add event or artist') , crumbs)

    addCityToForm()
    removeVenuesFromForm()
    form = MForm()
    tpl_data = {}
    return render_template(
        'muziekformulier.html',
        data=tpl_data,
        form=form,
        breadcrumbs=crumbs)


@bp.route('/showmethephotofileuploadformalready', methods=['GET', 'POST'])
def upload_form():
    if not current_user.is_authenticated:
        abort(403)
 
    form = PhotoForm()
    return render_template('muziek_upload_include.html', form=form)


@bp.route('/edituploadedphoto/<post_id>')
def edit_uploaded(post_id):

    logger=current_app.logger
    post=Post(post_id)
    post_ownership_check(post)
    form=PhotoForm()
    media=post.media
    if len (media):
        path_list=media[0]['local_path'].split('/')
        month_dir=path_list[0]
        file_name=path_list[-1]
        file_loc=month_dir + '_' + file_name
        media_id=media[0]['media_id']
        return render_template('muziek_upload_include.html', form=form, \
                file=file_name, file_loc=file_loc, media_id=media_id)
    else:
        return render_template('muziek_upload_include.html', form=form)


@bp.route('/uploadthephotofilealready', methods=['GET', 'POST'])
def upload():
    if not current_user.is_authenticated:
        abort(403)
 
    form = PhotoForm()
    if form.validate_on_submit():
        try:
            today = datetime.date.today() 
            month_dir = today.strftime('%Y-%m')
            file_dir = os.path.join(current_app.config['UPLOAD_FOLDER'], month_dir)
            upload_images = os.path.join(current_app.config['UPLOAD_ARCHIVE'], 'original', month_dir)
            upload_thumbs = os.path.join(current_app.config['UPLOAD_ARCHIVE'], 'thumb', month_dir)
            u.ensure_path(upload_images)
            u.ensure_path(upload_thumbs)
            u.ensure_path(file_dir)
            
            f = form.photo.data
            filename = secure_filename(f.filename)
            file_extension = filename.split('.')[-1].lower()
            session['file_id'] = u.uniqid() + '.' + file_extension
            file_loc = month_dir + '_' + session['file_id']
            file_path = os.path.join(file_dir, session['file_id'])
            f.save(file_path)
            original_file_path = os.path.join(upload_images, session['file_id'])
            formatted1 = u.format_image(file_path, original_file_path, 'original')
            file_abstract_path=os.path.join(month_dir, session['file_id'])
            media_id = sql.save_media(local_path=file_abstract_path)
            if not formatted1:
                form.photo.errors.append(_('error - could not process image'))
                return render_template('muziek_upload_include.html', form=form)
            return render_template('muziek_upload_include.html', form=form, \
                        file=filename, file_loc=file_loc, media_id=media_id)
        except Exception as e:
            form.photo.errors.append(_('error - could not process image'))
            logger = current_app.logger
            logger.exception(e)

            return render_template('muziek_upload_include.html', form=form)
    else:
        return render_template('muziek_upload_include.html', form=form)
