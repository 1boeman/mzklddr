import os
import json
from datetime import datetime
from datetime import date
import re
import time
import logging
from flask import Blueprint,render_template,g,abort,current_app,request,url_for
from flask_babel import format_datetime,_
from mzklddr.models.venue import Venue
from mzklddr.helpers import sql
from mzklddr.helpers import collect
from mzklddr.helpers import util as u
from mzklddr.helpers.caching import cache


bp=Blueprint('gig', __name__)


@bp.route('/<gig_hash>')
@cache.cached(query_string=True)
def gig(gig_hash):
    _list = gig_hash.split('_')
    _hash = re.sub(r"[\.\\\/%]+", "", _list[0])
    date_list = _list[1].split('-')
    cache_dir = cache_dir_path(
        date_list[0],
        date_list[1],
        date_list[2])
    # try to get it from cache 
    cache_file = os.path.join(cache_dir, _hash)
    # if no cache or cache older than 1 day - try db 
    if os.path.isfile(cache_file):
        if (time.time() - os.path.getmtime(cache_file))/60/60/24 > 1:
            event = get_and_cache_event(_hash, cache_dir)
            if not event:
                # event no longer in db - stick to cache
                event = get_cached_event(cache_file)
        else:
            # cached event less than day old - use cache
            event = get_cached_event(cache_file)
    else:
        # no cache available - db or bust
        event = get_and_cache_event(_hash, cache_dir)

    if event and len(event):
        past = False
        if isinstance(event['date_start'], date):
            event_dt = datetime.combine(event['date_start'], datetime.min.time())
        else:
            event_dt = event['date_start'],

        delta = event_dt - datetime.now()
        if delta.days <= -2:
            past = True
        
        try:
            venue = Venue(event['venue_id'], address_id=event['address_id'])
        except:
            venue_id = collect.diverse_locaties_venue_id(event['city_id'],True) 
            venue = Venue(venue_id)

        if request.args.get('jx'):
            return render_template('gig_detail_ajax.html', event=event, venue=venue, past=past)
        else:
            crumbs = u.crumb(url_for('agenda.list'), _('Calendar'))
            u.crumb(url_for('gig.gig', gig_hash=gig_hash), event['event_title'], crumbs)
            return render_template('gig.html', event=event, venue=venue, past=past,\
                breadcrumbs=crumbs)
    else:
        # if not in db and no cache - 404
        abort(404)


def get_cached_event(cache_file):
    logger = current_app.logger
    logger.debug(f"get_cached_event {cache_file}")
 
    try:
        with open(cache_file) as f:
            event = json.load(f)
            event['date_start'] = datetime.fromisoformat(event['date_start'])
            return event
    except Exception as e:
        logger = current_app.logger
        logger.exception(e)

        return None


def get_and_cache_event(_hash, cache_dir):
    logger = current_app.logger
    logger.debug(f"get_and_cache_event {_hash}")
    def json_serial(obj):
        """JSON serializer for objects not serializable by default json code"""
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        raise TypeError ("Type %s not serializable" % type(obj))

    event = sql.get_event(event_hash=_hash)
    if event:
        u.ensure_path(cache_dir)
        with open (os.path.join(cache_dir, _hash), 'w') as fp:
            json.dump(event, fp, sort_keys=True, default=json_serial)

    return event


def cache_dir_path(year, month, day):
    cache_dir = current_app.config['EVENT_CACHE']
    ymd = [str(int(x)) for x in [year, month, day]]
    return os.path.join(cache_dir, ymd[0], ymd[1], ymd[2]) 
