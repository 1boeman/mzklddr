from flask import current_app
import logging
from mzklddr.helpers import sql
from mzklddr.helpers import collect
from mzklddr.helpers import util as u
import json
import re


class Post():
    def __init__(self, post_id=None):
        self._post_id = None
        if post_id:
            self.load(post_id)


    def load(self, post_id):
        self._row = sql.get_post(post_id)
        if self._row:
            self._user_id = self._row['user_id']
            self._post_id = self._row['post_id']
            self._media = sql.get_post_media(post_id)
            self._title = self._row['post_title']
            self._post_type = self._row['post_type']


    @property
    def user_id(self):
        if self._post_id:
            return self._user_id
        return None


    @property
    def user(self):
        if self._post_id:
            return sql.get_user(user_id=self._user_id)
        return None


    @property
    def detail_link(self):
        if self._post_id:
            title_string = re.sub(r'\W+', '-', self._title)
            return "_".join([str(self._post_id), title_string.lower()])


    @property
    def post_id(self):
        return self._post_id


    @property
    def post_title(self):
        return self._title


    @property
    def post_type(self):
        return self._post_type


    @property
    def media(self):
        return self._media


    @property
    def post_data(self):
        if self.post_id:
            try:
                r =  json.loads(self._row['post_data'])
                return r
            except Exception as e:
                logging.debug('loading post json failed')
                logging.exception(e)
                return {}
        else:
            return {}


    @property
    def form_object(self):
        data = self.post_data
        obj = Form_data_container(initial_data=data)
        return obj


    @property
    def artist_details(self):
        props = {}
        if self._row:
            props = self.post_data
        return props


    @property
    def event_details(self):
        props = {}
        try:
            if self._row:
                pdata = self.post_data
                props['dates'] = pdata['eventDates'].split(",")
                props['type'] = pdata["eventType"]
                props["link"] = pdata['link']
                if "cityAsText" in pdata and pdata["cityAsText"]:
                    props['city'] = pdata["cityAsText"]
                else:
                    city = sql.get_city(pdata['citySelect'])
                    props['city'] = city['city_name']
                    props['city_id'] = city['city_id']

                if "venueAsText" in pdata and pdata['venueAsText']:
                    props['venue'] = pdata["venueAsText"]
                else:
                    props['venue'] = ''
                    if isinstance (pdata["venueSelect"], str):
                        id_list = pdata["venueSelect"].split('_')
                    else:
                        id_list = pdata["venueSelect"][0].split('_')
                    address_id = id_list[0]
                    venue_id = "_".join(id_list[1:])
                    venue_address = sql.get_venue_addresses(venue_id, address_id)
                    if venue_address:
                        props['venue_id'] = venue_address['venue_id']
                        props['venue'] = venue_address["venue_address_title"]
                        props['street_name'] = venue_address['street_name']
                        props['number'] = venue_address['number']
                        props['city_name'] = venue_address['city_name']


        except Exception as e:
            logging.exception(e)
            return props
        return props


    def publish(self, city_id_arg=None):
        # check if event is present in post_has_event table
        events = sql.get_post_event(self.post_id)
        if len(events):
            # if so delete old versions
            sql.delete_post_events(self.post_id)
        # insert events
        event_details = self.event_details
        if not "venue_id" in event_details:
            if city_id_arg:
                city_id = city_id_arg
            elif "city_id" in event_details:
                city_id = event_details['city_id']
            else:
                # cannot publish without city_id
                raise Exception("cannot publish event without city_id")
            venue_id = collect.diverse_locaties_venue_id(city_id)
            if not venue_id:
                venue_id = collect.create_diverse_locaties(city_id)

        else:
            venue_id = event_details['venue_id']
        
        venue_address = sql.get_venue_addresses(venue_id)
        # @todo - if there is more than one venue_address - 
        # the event form should offer options
        # and this function should be fed the chosen venue_address
        address_id = venue_address[0]['address_id']

        post_data = self.post_data
        for _date in event_details['dates']:
            sql_date = u.post_event_date_to_iso(_date)
            desc = post_data['descText'] if len(post_data['descText']) else "-" 
            desc = re.sub(r'\n+',' || ',desc)
            event_id = sql.save_event(
                event_title=self.post_title,
                date_start=sql_date,
                event_description=desc,
                event_link=post_data['link'],
                venue_id=venue_id,
                address_id=address_id
            )
            sql.save_post_event_link(self.post_id, event_id)

        # set published property to 1
            sql.save_user_post(post_id=self.post_id, published=1)
        # update the solr index here or elsewhere?


    
    def delete(self):
        if self.post_id:
            sql.delete_post_events(self.post_id)  
            return sql.delete_post(self.post_id)
        else:
            raise Exception("post_id not set") 


    def save_single_media(self, media_id):
        if self.post_id:
            # check if post_id media_id link already exists
            link_exists = False 
            post_medias = sql.get_post_media(self.post_id)
            for m in post_medias:
                if int(m['media_id']) == int(media_id):
                    link_exists = True 

            if not link_exists:
                sql.delete_post_media(self.post_id)
                sql.save_media_post(media_id, self.post_id)
        else:
            raise Exception("post_id not set") 


    def delete_media(self):
        if self.post_id:
            sql.delete_post_media(self.post_id)
        else:
            raise Exception("post_id not set") 


    def save_user_post(self, **kw):
        arguments= {
            "user_id": kw['user_id'],
            "post_title":kw['post_title'], 
            "post_data":kw['post_data'],
            "post_type":kw['post_type'],
            "published":kw['published']
        }
        if self.post_id:
            arguments['post_id']=self.post_id
            sql.save_user_post(**arguments)
            self.load(self.post_id)
        else:
            post_id = sql.save_user_post(**arguments)
            self.load(post_id)


class Form_data_container:
    def __init__(self, initial_data=None):
        if initial_data:
            for key in initial_data:
                setattr(self, key, initial_data[key])


    def __getattr__(self, name):
        return None


    def toJSON(self):
        return json.dumps(
            self,
            default=lambda o: o.__dict__, 
            sort_keys=True,
            indent=4)
