from flask import current_app
from mzklddr.helpers import sql


class Venue():
    def __init__(self, venue_id, address_id=None):
        self.logger = current_app.logger
        self.venue_id = venue_id
        self.address_id = address_id
        self.logger.debug(f"retrieve venue {venue_id}")
        self.venue_row = sql.get_venue(venue_id)
        if self.venue_row:
            self._title = self.venue_row['venue_title']
            self.venue_address_rows = [] # all venue_adresses for this venue
            self.venue_address_row = None
            self.logger.debug(self.venue_row)
            if address_id:
                self.set_venue_address(address_id)
        else:
            raise Exception(f"venue with venue_id {venue_id} not found in db")


    @property
    def title(self):
        return self._title


    @property
    def venue_title(self):
        return self._title


    @property
    def deactivated(self):
        if self.venue_row['venue_deactivated']:
            return True
        return False


    def get(self, prop, sub_object):
        self_dict = self.__dict__
        if hasattr(self, sub_object) and prop in self_dict[sub_object]:
            return self_dict[sub_object][prop]
        return None


    def get_venue_addresses(self):
        if not len(self.venue_address_rows):
            self.venue_address_rows = sql.get_venue_addresses(self.venue_id)
        return self.venue_address_rows


    def set_venue_address(self, address_id):
        self.address_id = address_id
        self.get_venue_addresses()
        for s in self.venue_address_rows:
            if s['address_id'] == address_id:
                self.venue_address_row = s
                self.logger.info(s)
                break


    def get_description(self,lang):
        self.description = None
        if lang != current_app.config['BABEL_DEFAULT_LOCALE']:
            if self.venue_address_row:
                self.description = sql.get_venue_address_translations(self.venue_address_row['venue_id'],self.address_id, lang)
            if not self.description:
                self.description = sql.get_venue_translations(self.venue_id, lang)

        if not self.description:
            if  self.venue_address_row and self.venue_address_row['venue_address_description']:
                self.description = self.venue_address_row['venue_address_description']

            else:
                self.description =  self.venue_row['venue_description']
        if not self.description:
            self.description ='-'
        return self.description


