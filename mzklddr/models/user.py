from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash
from flask_login import UserMixin
from mzklddr.helpers import sql
import logging


class User(UserMixin):
    def __init__(self, user_id=None, email=None):
        user_row = []
        if user_id:
            user_row = sql.get_user(user_id=user_id)
        elif email:
            user_row = sql.get_user(email=email)
        if user_row:
            self._user_id = user_row['user_id']
            self._email = user_row['email']
            self._user_name = user_row['user_name']
            self._password_hash = user_row['password']
            self._last_login = user_row['last_login']
            self._date_created = user_row['user_date_created']


    @property 
    def email(self):
        return self._email


    @email.setter 
    def email(self, email):
        self._email = email


    @property 
    def id(self):
        try:    
            return self._user_id
        except:
            return None


    @property 
    def user_id(self):
        try:
            return self._user_id
        except:
            return None


    @property 
    def user_name(self):
        try:
            return self._user_name
        except:
            return None


    @user_name.setter
    def user_name(self, user_name):
        self._user_name = user_name


    @property 
    def date_created(self):
        try:
            return self._date_created
        except:
            return None


    @property
    def password(self):
        raise AttributeError("Password is write-only")


    @password.setter
    def password(self, password):
        self._password_hash = generate_password_hash(password)


    @property
    def authentication (self):
        if self._user_id:
            return sql.get_user_authentication(self._user_id)
        else:
            return []

    @authentication.setter
    def authentication (self, value):
        if self._user_id:
            if value:
                sql.save_user_authentication(self._user_id, 1)
            else:
                sql.save_user_authentication(self._user_id)


    def check_password(self, password):
        return check_password_hash(self._password_hash, password)


    def check_drupal_password(self,password):
        from drupal_hash_utility import DrupalHashUtility
        try:
            drash = DrupalHashUtility()
            return drash.verify(password, self._password_hash)
        except:
            return False

    def update_last_login(self):
        if self.user_id:
            sql.user_last_login(self.user_id)


    def save(self):
        sql.save_user(
            user_id = self._user_id,
            email=self._email,
            user_name=self._user_name,
            password=self._password_hash
        )
