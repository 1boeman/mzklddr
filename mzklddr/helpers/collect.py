import base64
import re
import math
import zlib
import logging
from flask import url_for, current_app
from . import sql
from . import util as u
from mzklddr.models.post import Post
from flask_login import current_user
from mzklddr.helpers.solr.client import select, get_solr, index_doc


def search_index_post(post_id):
    data = {}
    # add dates and post_id+i
    post = Post(post_id)
    if not post.post_id:
        return data

    data['data_type_s'] = 'post'
    data['post_type_s'] = post.post_type 
    data['id'] = "post_id_" + str(post.post_id)
    data['post_id_i'] = post.post_id
    data['user_id_i'] = post.user_id
    user = sql.get_user (user_id = post.user_id)
    data['user_name_s'] = user['user_name']
    data['post_title_t'] = post.post_title
    pdata = post.post_data
    data['desc_text_t'] = pdata['descText']
    if post.post_type == 'event':
        from datetime import datetime 
        data['event_title_t'] = post.post_title
        event_details = post.event_details
        data["city_s"] = event_details['city']
        if "city_id" in event_details:
            data['city_id_i'] = event_details['city_id']
        data['venue_title_s'] = event_details['venue']
        if "street_name" in event_details:
            data['address_s'] =  data['venue_title_s'] 
            if "number" in event_details:
                data['address_s'] += ' ' + event_details["number"]

        data['date_start_dts'] = []
        for d in event_details['dates']:
            sql_date = u.post_event_date_to_iso(d)
            _date = datetime.fromisoformat(sql_date)
            data['date_start_dts'].append(_date.strftime('%Y-%m-%dT00:00:00Z'))
    index_doc(data)
    return data


def get_nearby_cities(city_row, max_distance=10):
    solr = get_solr
    if "city_geo" in city_row:
        try:
            params = {
                "q":"*:*",
                "fq":"{!geofilt}",
                "sfield":"city_geo_p",
                "pt":city_row["city_geo"],
                "rows":999,
                "d":max_distance}
            r = select(params)
            if "response" in r \
                and "docs" in r["response"] \
                    and len(r["response"]['docs']):
                rval = [d["city_i"] for d in r["response"]['docs']]
                return rval 
            else: 
                return [city_row["city_id"]]
        except Exception as e:
            logging.exception(e)
    return [city_row["city_id"]]

def diverse_locaties_venue_id(city_id, even_if_not_exist=False):
    city = sql.get_city(city_id);
    if not city:
        raise Exception(f"city_id {city_id} not found in db")

    city_name = re.sub('[^0-9a-zA-Z\.\-]+', '_', city['city_name'])
    venue_id = "_".join([str(city_id), city_name])
    if even_if_not_exist:
        return venue_id

    if sql.get_venue(venue_id):
        return venue_id
    else:
        return None


def create_diverse_locaties(city_id):
    venue_id = diverse_locaties_venue_id(city_id, False)
    if not venue_id:
        venue_id = diverse_locaties_venue_id(city_id, True)
        city = sql.get_city(city_id);
        venue_title = city['city_name'] + ' diverse locaties'
        street_id = sql.save_street('onbekend', city['city_id'])
        address_id = sql.save_address(street_id)
        sql.save_venue(venue_id, venue_title)
        venue_address = sql.save_venue_address(
            address_id, venue_id, venue_title)
    return venue_id


def can_edit(post):
    if current_user.is_authenticated and post.user_id == current_user.user_id:
        return True
    return False


def post_listing(page=0, user_id=None):
    if page:
        page = int(page)
    posts = post_list(page=page, user_id=user_id, get_total=True)
    if 'total' in posts and 'total' in posts['total']:
        total_posts = int(posts['total']['total'])
        total_pages = math.ceil(
            total_posts /
            current_app.config['POSTS_PER_PAGE'])
    else:
        total_posts = 0
        total_pages = 0
    
    data = {
        "posts":posts['post_list'],
        "page": page,
        "page_range": page_range(page, total_pages),
        "total_posts": total_posts,
        "total_pages": total_pages
    }
    return data


def page_range(page, total_pages):
    stretch = 6
    if page + stretch > total_pages:
        range_forward = list(range(page+1, total_pages+1))
    else:
        range_forward = list(range(page+1, page + stretch+1))
    
    if page - stretch < 0:
        range_back = list(range(1,page+1))
    else: 
        range_back = list(range(page+1-stretch,page+1))
    
    page_range = range_back + range_forward
    page_range = list(set(page_range))
    page_range.sort()
    return page_range


def post_list(page=0, get_total = False, user_id=None, post_ids=[]):
    posts = sql.user_post_list(
        page=page, 
        user_id=user_id, 
        post_ids=post_ids)
    post_list = []
    for p in posts:
        post_list.append(Post(p['post_id'])) 
    if get_total:
        return {"total":sql.count_user_posts(user_id=user_id),
                "post_list":post_list}
    return post_list


def get_city_grouping(group_id):
    data = {}
    response = sql.get_city_groupings(group_id)
    data['group'] = response[0]
    data['group_translations'] = {}
    for r in response:
        if r['lang_code']:
            data['group_translations'][r['lang_code']]=r['string']
    return data


def post_link(post_id):
    p = Post(post_id)
    slug = p.detail_link
    return url_for('tips.tip', post_id_slug=slug)


def gig_link(_hash, date_start, venue_id):
    if isinstance(date_start, str):
        date_start = date_start[:10]
    else:
        date_start = date_start.strftime("%Y-%m-%d")
    return url_for(
        'gig.gig',
        gig_hash='_'.join([_hash, date_start, venue_id]))


def venue_link(venue_id):
    return url_for('locaties.venue', venue_id=venue_id)


def city_link(city_id, city_name=None):
    if not city_name:
        city = sql.get_city(city_id)
        city_name = city['city_name']
    return url_for('uitgaan.city', city_id='-'.join([str(city_id), city_name]))


def city_agenda_link(city_id, city_name):
    return url_for('agenda.list', location_or_day='-'.join([str(city_id), city_name]))


def img_url_to_hash(img_url):
    _hash = base64.urlsafe_b64encode(zlib.compress(img_url.encode("utf-8"),9))
    return _hash.decode()


def img_url_from_hash(img_hash):
    url = zlib.decompress(base64.urlsafe_b64decode(img_hash))
    return url.decode("utf-8")


def country_code(_name):
    country_codes = {   "Netherlands":"NL",
                        "Belgium":"BE",
                        "Germany":"DE"}
    if _name in country_codes:
        return country_codes[_name]
    return _name


def country_name(_name, language):
    country_names = {"nl":
                {"Netherlands":"Nederland",
                 "Belgium":"België",
                 "Germany":"Duitsland"}}
    if language in country_names:
        if _name in country_names[language]:
            return country_names[language][_name]
    return _name


