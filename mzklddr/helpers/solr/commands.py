import click
import requests
import json
import xml.etree.ElementTree as ET
from flask.cli import with_appcontext
from flask import current_app, g
from .client import get_solr_config, get_solr_schema


def init_app(app):
    app.cli.add_command(solr_config)
    app.cli.add_command(solr_schema)


@click.command('solr-config')
@click.argument('input_file', type=click.Path(exists=True))
@with_appcontext
def solr_config(input_file):
    solr_config = get_solr_config()
    click.echo(click.format_filename(input_file))
    with click.open_file(input_file) as j:
        data = json.load(j)
        for command in data:
            click.secho (command,fg='blue')
            r = requests.post(solr_config,json=command)
            click.secho(r,fg='green')
        click.echo('Configured solr_config')



@click.command('solr-schema')
@click.argument('input_file', type=click.Path(exists=True))
@with_appcontext
def solr_schema(input_file):
    def solr_schema_request(command,data):
        solr_schema = get_solr_schema()
        call =  {command:data}
        r = requests.post(solr_schema,json=call )
        click.secho(r,fg='green')

    with click.open_file(input_file) as f:
        root = ET.fromstring(f.read()) 
        for field in root:
            if field.tag == 'field':
                click.secho(field.attrib['name'])
                solr_schema_request("delete-field",{"name" : field.attrib['name']})
                solr_schema_request("add-field",field.attrib)
            elif field.tag == 'copy-field':
                solr_schema_request("delete-copy-field",field.attrib)
                solr_schema_request("add-copy-field",field.attrib)
        click.echo('Updated schema')
