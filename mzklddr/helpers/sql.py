from . import db
import logging
from flask import current_app,g
import json
import hashlib


def query(sql, arguments=[], one=False):
    r= []
    try:
        cursor = db.get_cursor()
        cursor.execute(sql, arguments)
        if one:
            r = cursor.fetchone()
        else:
            r = cursor.fetchall()
        cursor.close()
    except Exception as e:
        logging.debug(f"Exception query: {sql}")
        logging.debug("Exception arguments: " + str(arguments))
 
        logging.exception(e)
    return r


def save(sql, arguments=[], get_insert_id=True):
    r = 0
    try:
        cursor = db.get_cursor()
        logging.debug(sql) 
        cursor.execute(sql, arguments)
        db.do_commit()
        if get_insert_id:
            r = cursor.lastrowid
        else:
            r = cursor.rowcount
        cursor.close()
    except Exception as e:
        logging.debug(f"Exception save: {sql}")
        logging.debug("Exception arguments: " + str(arguments))
 
        logging.exception(e)
    return r


def get_user(**kwargs):
    if "user_id" in kwargs:
        q = 'select * from user where user_id = %s'
    elif "user_name" in kwargs:
        q = 'select * from user where user_name = %s'
    elif "email" in kwargs:
        q = 'select * from user where email = %s' 
    else:
        raise Exception("Please specify criterium for user")

    return query(q, (list(kwargs.values())[0],), one=True)


def user_last_login(user_id):
    q='update user set last_login = NOW() where user_id = %s'
    return save(q,(user_id,),get_insert_id=False)


def save_user(**kw):
    if not "user_id" in kw:
        q = """
            insert into user 
            (email, user_name, password, active, user_date_created) 
            values (%s, %s, %s, %s, NOW()) 
        """
        values =  (kw['email'], kw['user_name'], kw['password'], kw['active'])
    else:
        user_id = kw.pop('user_id')
        values = list(kw.values())
        qa = []
        q = 'update user set '
        for k in kw:
            qa.append(k+' = %s')
        q += ','.join(qa)
        q += ' where user_id = ' + str(user_id)

    return save(q, values)


def get_user_authentication(user_id):
    q = """ select * from authentication 
                where auth_id in 
                (select auth_id from user_has_authentication 
                    where user_id = %s)         
        """
    return query(q,(user_id,))


def save_user_authentication(user_id, *auth_ids):
    save('delete from user_has_authentication where user_id = %s',(user_id,))
    if len(auth_ids):
        for auth_id in auth_ids:
            save ("""insert into user_has_authentication (user_id, auth_id)
                    values(%s, %s) """,(user_id, auth_id))


def get_post_event(post_id):
    q = "select event_id from post_has_event where post_id = %s"
    if events := query(q, [post_id,]):
        return [get_event(event['event_id']) for event in events]
    else:
        return []


def save_post_event_link(post_id, event_id):
    sql = """insert into post_has_event(post_id, event_id)
                values(%s,%s)
            """
    save(sql, (post_id, event_id))
    return [post_id, event_id]


def delete_post_events(post_id):
    sql = 'select event_id from post_has_event where post_id = %s'
    event_ids = query(sql,(post_id,))     
    save('delete from post_has_event where post_id = %s', (post_id,))
    for row in event_ids:
        save('delete from event where event_id = %s', (row['event_id'],)) 


def save_event(**kw):
    logger = current_app.logger
    _hash_keys =  [ 'event_title',
                    'date_start',
                    'event_description',
                    'event_link',
                    'venue_id',
                    'address_id'] # used to build hash identifier
    needed_keys = [ 'event_title',
                    'date_start',
                    'event_description',
                    'event_link',
                    'venue_id',
                    'address_id'] # minimum required keys
    insert_keys = [ 'event_title',
                    'date_start',
                    'time_start',
                    'date_end',
                    'time_end',
                    'event_description',
                    'event_link',
                    'venue_id',
                    'address_id',
                    'event_hash' ]
    _hash_list = []
    values = [] 
    for k in needed_keys:
        if k not in kw or not kw[k]:
            msg = f"Missing kw_argument {k} in save_event"
            logger.debug(msg)
            logger.debug(kw)
            raise Exception(msg)

    for k in _hash_keys:
        _hash_list.append(str(kw[k]))
        _hash = hashlib.sha256("".join(_hash_list).encode("utf-8")).hexdigest()

    for k in insert_keys:
        if k in kw:
            values.append(kw[k])
        elif k == 'event_hash':
            values.append(_hash)
        else:
            values.append(None)
 
    keystring = ",".join(insert_keys)
# insert event
    sql = ' insert into muziekladder_db.event (' + keystring + ') values ( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s ); '
    event_id = save(sql, values)
    return event_id


def count_user_posts(published=False, user_id=None):
    q = 'select count(*) as total from post p where 1 '
    arguments = []
    if published:
        q+= ' and p.published = 1 ' 
    if user_id:
        q+= ' and user_id = %s'
        arguments.append(user_id)

    return query(q, arguments, one=True)


def get_last_post_id(limit):
    return query('select post_id from post order by post_id desc limit %s',(limit,))


def user_post_list( published=False,
                    not_published=False,
                    post_type=None,
                    unpack_data=False, user_id=None,
                    page=0, post_ids=[]):
    # ppp aka posts per page
    offset = current_app.config['POSTS_PER_PAGE'] * int(page)
    q = 'select * from post p where 1 '
    arguments = []
    if published:
        q+= ' and p.published = 1 '
    if not_published:
        q+= ' and p.published = 0 '
    if post_type:
        q+= ' and p.post_type = %s '
        arguments.append(post_type)
    if user_id:
        q+= ' and p.user_id = %s '
        arguments.append(user_id)
    if len(post_ids):
        q+= ' and p.post_id in (%s) '
        arguments.append(",".join(map(str,post_ids)))

    q += ' order by p.last_update desc, p.date_created desc'
    q += ' limit %s offset %s'

    arguments.extend([current_app.config['POSTS_PER_PAGE'], offset])
    posts = query(q, arguments)
    if unpack_data:
        for p in posts:
            p['post_data'] = json.loads(p['post_data'])
    return posts


def get_post(post_id):
    q = 'select * from post where post_id = %s'
    return query(q,(post_id,),one=True)


def save_user_post(**kw):
    if not "post_id" in kw:
        q = """
            insert into post 
            (user_id, post_title, post_data, post_type, published, 
                last_update, date_created) 
            values (%s, %s, %s, %s, %s, NOW(), NOW()) 
        """
        values =  (kw['user_id'], kw['post_title'], kw['post_data'],
                    kw['post_type'], kw['published'])
    else:
        post_id = kw.pop('post_id')
        values = list(kw.values())
        qa = []
        q = 'update post set '
        for k in kw:
            qa.append(k+' = %s ')
        q += ','.join(qa)
        q += ' where post_id = ' + str(post_id)

    return save(q, values)


def get_media(**kw):
    if "url" in kw:
        q = 'select * from media where url = %s'
        return query(q,(kw['url'],), one=True)
    elif "media_id" in kw:
        q = 'select * from media where media_id = %s'
        return query(q, (kw['media_id'],), one=True)
    return []



def save_media(local_path=None, url=None, MIME=None, **kw):
    if not "post_id" in kw:

        q = """
            insert into media 
            (url, local_path, MIME) 
            values (%s, %s, %s) 
        """
        return save(q, (url, local_path, MIME))



def get_post_media(post_id):
    q = """
        select pm.media_id, m.* from media m
            natural join post_has_media pm
            where pm.post_id = %s 
    """
    return query(q, (post_id,))



def delete_post(post_id):
    delete_post_media(post_id)
    sql = "delete from post where post_id = %s"
    return save(sql,(post_id,), get_insert_id=False)


def delete_post_media(post_id):
    media_id = None
    media = get_post_media(post_id)
    for m in media:
        media_id = m['media_id']
        q = """delete from post_has_media where media_id = %s 
                and post_id = %s
        """
        r=save(q,(m['media_id'],post_id), get_insert_id=False)
    
    if media_id:
        q = "delete from media where media_id = %s"
        r=save(q,(media_id,), get_insert_id=False)

    return media


def save_media_post(media_id,post_id):
    q = 'insert into post_has_media (post_id,media_id) values(%s,%s)'
    return save(q,(post_id,media_id)) 


def get_events( date_start=0,
                date_range_end=0,
                venue_id=0,
                city_id=0,
                multi_city_ids=[],
                group_id=0,
                start=0,
                rows=0):

    logger = current_app.logger
    if rows == 0:
        rows = current_app.config['RESULTS_PER_PAGE']
    arguments = []
    q = ''' select e.*, v.*, va.*, a.*, s.*, c.*, co.*, 
            m.MIME as media_mime, m.url as media_url, m.local_path as media_local_path
            from event e
            inner join venue_address  va
                on va.address_id = e.address_id
                and va.venue_id = e.venue_id
            inner join venue v on v.venue_id = e.venue_id
            inner join address a on a.address_id = e.address_id
            inner join street s on s.street_id = a.street_id
            inner join city c on c.city_id = s.city_id
            inner join country co on co.country_id = c.country_id
        '''
    if group_id:
        q+= '''
            inner join city_groupings_has_city cghc
                on c.city_id = cghc.city_id
                inner join city_groupings cg
                on cg.group_id = cghc.group_id
                and cg.group_id = %s
            '''
        arguments.append(group_id)

    q+= ' left outer join event_has_media ehm on ehm.event_id = e.event_id '
    q+= ' left outer join media m on ehm.media_id = m.media_id '

    q+= ' where 1 '
    if date_start:
        q+= ' and e.date_start >= %s '
        arguments.append(date_start)
    if date_range_end:
        q+= ' and e.date_start <= %s '
        arguments.append(date_range_end)
    if venue_id:
        q+= ' and v.venue_id = %s '
        arguments.append(venue_id)
    if city_id:
        q+= ' and c.city_id = %s '
        arguments.append(city_id)
    if len(multi_city_ids):
        format_strings = ','.join(['%s'] * len(multi_city_ids))
        q+= ' and c.city_id in (' + format_strings + ') '
        arguments.extend(multi_city_ids)

    q+=' order by e.date_start asc, c.city_name '
    q += ' limit %s '
    arguments.append(rows)

    if start:
        q+= ' offset %s '
        arguments.append(start)

    result = query(q, arguments)
    fix_bytearray(result)
    return result


def save_city_groupings_has_city(group_id, city_id):
    sql = """insert into city_groupings_has_city 
            (group_id, city_id) values (%s,%s)"""
    return  save(sql, (group_id, city_id)) 


def get_city_groupings(group_id=None):
    if group_id:
        return query('''select cg.*,ts.* from city_groupings cg
                        natural left join city_groupings_has_translation_string cghts
                        natural left join translation_string ts
                        where group_id = %s
                        ''', (group_id, ))
    else:
        return query('''select cg.*,ts.* from city_groupings cg
                        natural left join city_groupings_has_translation_string cghts
                        natural left join translation_string ts
                        order by cg.sort_order asc''')


def get_city(city_id):
    return query('''select c.*, cn.*, cg.*
                    from city c
                    natural left join country cn
                    left join city_groupings_has_city cghc 
                        on cghc.city_id = c.city_id
                    left join city_groupings cg
                        on cg.group_id = cghc.group_id
                      where c.city_id = %s ''', ( city_id, ), one=True)


def get_cities_with_names_containing_dash():
    return query ("SELECT * from city WHERE city_name like '%-%'")


def get_all_locations():
    city_groupings = get_city_groupings()
    cities = get_cities(show_by_groupings=True)
    venues = query('''
        select v.*, c.city_id,c.city_name
            from venue v
            natural join venue_address va
            natural join address a
            natural join street s
            natural join city c ''')
    return { "cities":cities,"venues":venues,"city_groupings":city_groupings }


def get_streets():
    return query ("select s.*,c.* from  street s natural join city c")


def get_country(country_id):
    return query("select * from country where country_id =%s", 
        (country_id,), one=True)


def get_cities_by_ids(city_ids=[]):
    ids = [str(city_id) for city_id in city_ids]
    str_ids = ",".join(ids)
    return query('''select * from city 
                        natural join country 
                        where city_id in (''' + str_ids + ''') 
                        order by country_id desc ,city_name''')


def get_cities(show_by_groupings = False, group_id=False):
    if show_by_groupings:
        return query('''
                     select c.*,co.*,cg.group_name,cg.group_id from city c
                        natural join country co
                        left join city_groupings_has_city cghc on cghc.city_id = c.city_id
                        left join city_groupings cg on cghc.group_id = cg.group_id
                        order by co.country_id desc, cg.group_id, c.city_name''')
    elif group_id:
        return query('''select * from city c

                        inner join city_groupings_has_city cghc 
                            on cghc.city_id = c.city_id
                        inner join city_groupings cg
                            on cghc.group_id = cg.group_id
                        natural join country

                        where cg.group_id = %s
                        order by country_id desc, city_name''', (group_id,))
    else:
        return query('select * from city natural join country order by country_id desc, city_name')


def get_city_venues(city_id, active_only=True):
    q = """
      select v.*,va.*,a.*,s.*, c.city_id,c.city_name 
          from venue v 
          inner join venue_address va on va.venue_id = v.venue_id
          inner join address a on va.address_id = a.address_id
          inner join street s on s.street_id = a.street_id
          inner join city c on c.city_id = s.city_id
          where s.city_id = %s
    """
    if active_only:
        q += ' and v.venue_deactivated is NULL '    
    q += ' order by v.venue_title,v.venue_id asc '

    result = query(q, ( city_id, ))
    fix_bytearray(result)
    return result


def fix_bytearray(result):
    #  mysql.connector returns this varchar as bytearray (bug?)
    if type(result) == list:
        for r in result:
            if (type(r['address_geo']) == bytearray):
                r['address_geo'] = r['address_geo'].decode()
    elif type(result) == dict:
        if (type(result['address_geo']) == bytearray):
            result['address_geo'] = result['address_geo'].decode()


def get_venue(venue_id):
    q = '''
        select v.* 
            from venue v  
            where v.venue_id = %s
    '''
    logging.debug(venue_id)
    return query(q,(venue_id,),one=True)


def get_venue_addresses(venue_id, address_id=None):
    logger = current_app.logger
    q = """select * from venue_address va
            natural join address a
            natural join street s
            natural join city c
            natural join country co
            where venue_id = %s"""
    if address_id:
        q += 'and va.address_id = %s'
        result = query(q,(venue_id,address_id), one=True)
    else:
        result = query(q,(venue_id,))
    fix_bytearray(result)
    return result


def get_venue_translations(venue_id, lang=None):
    q = '''
        select v.venue_id, v.venue_title, tt.* from venue v
            inner join venue_has_translation_description vtd 
                on vtd.venue_id = v.venue_id
            inner join translation_text tt
                on vtd.translation_id = vtd.translation_id
                where v.venue_id = %s
    '''
    if lang:
        q+=' and tt.lang_code = %s'
        return query(q, (venue_id, lang), one=True)

    return query(q, (venue_id,))


def get_venue_address_translations(venue_id, address_id, lang=None):
    q = '''
        select va.venue_id, va.address_id from venue_address va
            inner join venue_address_has_translation_description vatd
                on vatd.venue_id = va.venue_id and vatd.address_id = va.address_id
            inner join translation_text tt
                on tt.translation_id = vatd.translation_id
                where va.venue_id = %s and va.address_id = %s
        '''
    if lang:
        q+=' and lang_code = %s'
        return query(q, (venue_id, address_id, lang), one=True)

    return query(q,(venue_id, address_id))


def update_city(city_id, **kw):
    values = list(kw.values())
    values.append(city_id)
    qa = []
    q = 'update city set '
    for k in kw:
        qa.append(k+' = %s ')
    q += ','.join(qa)
    q += ' where city_id = %s'
    return save(q, values)


def save_city(city_id, city_name, country_id):
    sql = "insert into city (city_id, city_name, country_id) value (%s, %s, %s)"
    return  save(sql, (city_id, city_name, country_id)) 


def save_street(street_name, city_id):
    sql = "insert into street (city_id, street_name) values (%s,%s)"
    street_id = save(sql, (city_id, street_name))
    return street_id


def save_address(street_id, _zip=None, number=None, address_geo=None ):
    sql = """   insert into address (street_id,zip,number,address_geo) 
                values (%s,%s,%s,%s)
          """
    address_id = save(sql, (street_id, _zip, number, address_geo))
    return address_id


def save_venue(venue_id, title, description=None, link=None):
    sql = """   INSERT INTO venue 
                (venue_id, venue_title, venue_description, venue_link)
                VALUES (%s, %s , %s , %s);
          """
    save(sql,(venue_id, title, description, link))
    return venue_id


def save_venue_address( address_id, venue_id, 
                        venue_address_title=None,
                        venue_address_description=None):
    sql = """insert into venue_address 
                (address_id,venue_id,venue_address_title,
                venue_address_description) 
             values (%s,%s,%s,%s)"""

    save(sql,(  address_id, venue_id, 
                venue_address_title, 
                venue_address_description))
    return [address_id, venue_id]


def save_venue(venue_id, title, description=None, link=None):
    sql = """   INSERT INTO muziekladder_db.venue 
                (venue_id, venue_title, venue_description, venue_link)
                VALUES (%s, %s , %s , %s);
          """
    venue_id = save(sql,(venue_id, title, description, link))
    return venue_id


def get_event(event_id=None, event_hash=None):
    result = None
    q = '''
        select e.*,va.*,a.*,s.*,
          m.MIME as media_mime, m.url as media_url, m.local_path as media_local_path,
          a.number as street_number,c.*,co.*,v.* from event e
          natural join venue_address va
          natural join address a
          natural join street s
          natural join city c
          natural join country co
          natural join venue v
          left join event_has_media ehm on ehm.event_id = e.event_id
          left join media m on m.media_id = ehm.media_id
    '''
    if event_hash:
        q += ' where event_hash = %s '
        result = query(q,(event_hash,),one=True)
    elif event_id:
        q += ' where e.event_id = %s '
        result = query(q, (event_id,),one=True)

    fix_bytearray(result)

    return result
