import os
import re
import uuid
import ssl
import smtplib
from PIL import Image
from flask import current_app, request, abort
from flask_login import current_user
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def post_event_date_to_iso(date_str):
    _l = date_str.strip().split('-')
    _l[0] = _l[0].zfill(2)
    _l[1] = _l[1].zfill(2)
    return "-".join(reversed(_l))



def isValidUrl(string):
    url_pattern = "^https?:\\/\\/(?:www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b(?:[-a-zA-Z0-9()@:%_\\+.~#?&\\/=]*)$"
    if re.match(url_pattern, string):
        return True
    return False


def ownership_check(user_id):
    if not current_user.is_authenticated or \
            current_user.user_id != int(user_id):
        abort(403)


def make_key ():
  """Make a cache key that includes GET parameters."""
  return request.full_path


def dedup_events(events):
    """ dedup agenda events """
    seen = {}
    dedupped = []
    for e in events:
        _key = str(e['date_start'])
        if _key not in seen:
            seen[_key]={}
        if e['event_title'] not in seen[_key] or \
            seen[_key][e['event_title']]['street_name'] == 'onbekend':
            seen[_key][e['event_title']] = e

    for dt_strt in seen:
        for _title in seen[dt_strt]:
            dedupped.append(seen[dt_strt][_title])
    return dedupped


def crumb(link, text, crumbs=None):
    if not crumbs:
        crumbs = []
    crumbs.append({"link":link,"text":text})
    return crumbs


def uniqid():
    return str(uuid.uuid4())


def ensure_path(path):
    if not os.path.isdir(path):
        os.makedirs(path)
    return path


def get_user_tmp():
    user_tmp = os.path.join(current_app.config['USER_DATA_DIR'],'tmp')
    ensure_path(user_tmp)
    return user_tmp


def send_email(to, subject, body):
    cfg = current_app.config
    logger = current_app.logger
    try:
        logger.debug(f"sending mail to {to} with subject: {subject}")
        msg = MIMEMultipart()
        msg['From'] = cfg['FROM_MAIL']
        msg['To'] = to
        msg['Subject'] = subject
        message = body
        msg.attach(MIMEText(message))
        context = ssl.SSLContext(ssl.PROTOCOL_TLS)
        mailserver = smtplib.SMTP(cfg['SMTP_SERVER'],cfg['SMTP_PORT'])
        mailserver.ehlo()
        mailserver.starttls(context=context)
        mailserver.ehlo()
        mailserver.login(cfg['SMTP_USER'], cfg['SMTP_PASS'])
        mailserver.sendmail(cfg['FROM_MAIL'],to , msg.as_string())
        mailserver.quit()
        logger.debug(f"Successfully sent mail to {to} with subject: {subject}")
        return True
    except Exception as e:
        logger.debug(cfg['SMTP_SERVER'])
        logger.debug(cfg['SMTP_PORT'])
        logger.exception(e)
        return False


def format_image(path_or_pillow_im, dest_file, _format):
    formats = current_app.config['IMAGE_FORMATS']
    logger = current_app.logger
    if _format in formats:
        if isinstance(path_or_pillow_im, str):
            pillow_im = Image.open(path_or_pillow_im)
        else:
            pillow_im = path_or_pillow_im
        try:
            if _format == 'thumb':
                pillow_im.thumbnail(formats['thumb'])
            elif _format == 'original':
                logger.debug('original')
            else:
                pillow_im.thumbnail(formats[_format],Image.Resampling.LANCZOS)
            # make sure directory exists and write file
            dest_list = dest_file.split('/')
            dest_list.pop()
            ensure_path("/".join(dest_list))
            pillow_im.save(dest_file, quality=95, optimize=True)
            pillow_im.close()
            return True
        except Exception:
            logger.exception('%s format_image failed', dest_file)
            pillow_im.close()
            return False
    return False


def check_image_allowed(path_or_pillow_im):
    mimes = current_app.config['ALLOWED_MIMES']
    if isinstance(path_or_pillow_im, str):
        pillow_im =  Image.open(path_or_pillow_im)
    else:
        pillow_im = path_or_pillow_im
    _mimetype = pillow_im.get_format_mimetype()
    pillow_im.close()
    if _mimetype in mimes:
        return True
    return False


