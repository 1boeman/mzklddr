import mysql.connector as database
from flask import current_app,g


def init_app(app):
    app.teardown_appcontext(close_db)


def get_db():
    if 'db' not in g:
        cnx = database.connect(
            user=current_app.config['MYSQL_USER'],
            password=current_app.config['MYSQL_PASSWORD'],
            host=current_app.config['MYSQL_HOST'],
            database=current_app.config['MYSQL_DB'],
            use_pure=True
        )
        g.db = cnx
    return g.db


def get_cursor():
    db = get_db()
    return db.cursor(dictionary=True, buffered=True)


def do_commit():
    db = get_db()
    db.commit()


def close_db(e=None):
    db = g.pop('db', None)
    if db is not None:
        db.close()

