from flask import Blueprint, render_template, g,abort, current_app, url_for, redirect, abort
from flask_babel import format_datetime, _
from mzklddr.helpers import sql
from mzklddr.models.venue import Venue
from mzklddr.helpers import collect
from mzklddr.helpers import util as u
from mzklddr.helpers.caching import cache
import math


bp=Blueprint('uitgaan', __name__)


@bp.route('/')
@bp.route('/<city_id>')
@cache.cached()
def city(city_id = None):
    logger = current_app.logger
    if city_id:
        # redirect to id-name url if need be
        id_plus_name = city_id.split('-')
        if len (id_plus_name) < 2:
            _city_id = id_plus_name[0]
            city_row = sql.get_city(_city_id)
            if city_row:
                city_link = collect.city_link(_city_id, city_row['city_name'])
                return redirect(city_link)
            else:
                abort(404)
        return city_details(city_id)
    else:
        crumbs = u.crumb(url_for('uitgaan.city'), _('Locations'), [])
        data = sql.get_all_locations()
        data['city_list_breaks'] = [math.floor(len(data['cities'])/3), math.floor(2*(len(data['cities'])/3))]

        return render_template('uitgaan.html', data=data, breadcrumbs=crumbs)


def city_details(city_id):
    logger = current_app.logger
    crumbs = u.crumb(url_for('uitgaan.city'), _('Locations'), [])
    data  = {'venues' :[]}
    data['venue_rows'] =  sql.get_city_venues(city_id)
    city = sql.get_city(city_id)
    if not city: 
        abort(404)

    data['city'] = city
    u.crumb(collect.city_link(city['city_id'],city['city_name']), 
        city['city_name'], crumbs)

    for v in data['venue_rows']:
        venue =  Venue(v['venue_id']) 
        venue_addresses = venue.get_venue_addresses()
        if venue_addresses[0]['street_name'] != 'onbekend':
            data['venues'].append(venue)

    data['venues_list_breaks'] = [math.floor(len(data['venues'])/3), math.floor(2*(len(data['venues'])/3))]

    return render_template('uitgaan_city.html',
        data=data,
        breadcrumbs=crumbs)
