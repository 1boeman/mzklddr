import os
import shutil
from PIL import Image
import shutil
from pathlib import Path
import requests
from flask import Blueprint, g,abort, current_app, send_file, send_from_directory
from flask_babel import format_datetime
from mzklddr.helpers import sql
from mzklddr.helpers import collect
from mzklddr.helpers import util as u


bp=Blueprint('image', __name__)


request_headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/119.0'
}


def get_format_dir(_format):
    if _format in current_app.config['IMAGE_FORMATS'].keys():
        file_dir = os.path.join(current_app.config['UPLOAD_ARCHIVE'], _format)
        u.ensure_path(file_dir)
        return file_dir
    return None


@bp.route('/format/<_format>/<_media_id>')
def get_image(_format, _media_id):
    log = current_app.logger
    media = sql.get_media(media_id=_media_id)
    log.debug(media)
    if media and media['local_path']:
        path_list=media['local_path'].split("/")
        file_name=path_list[-1]
        month_dir=path_list[-2]
        path_to_original=os.path.join(get_format_dir('original'), month_dir, file_name)
        log.debug(os.path.isfile(path_to_original))
        current_app.logger.debug(path_to_original)
        if file_dir := get_format_dir(_format):
            file_month_dir=os.path.join(file_dir, month_dir)
            dest_file = os.path.join(file_month_dir, file_name)
            if os.path.isfile(dest_file):
                log.debug(f"{file_name} found")
                return send_from_directory(file_month_dir, file_name)
            elif u.format_image(path_to_original, dest_file, _format):
                return send_from_directory(file_month_dir, file_name)
    abort(404)


@bp.route('/from/<_format>/<filename>')
def image_from(_format, filename):
    if file_dir := get_format_dir(_format):
        month_dir = filename.split('_')[0]
        file_dir = os.path.join(file_dir, month_dir)
        filename = filename.split('_')[1]
        return send_from_directory(file_dir, filename)
    abort(404)


@bp.route('/<_format>/<_hash>')
def image(_format, _hash):
    logger = current_app.logger
    dest_file = ''
    formats = current_app.config['IMAGE_FORMATS']
    mimes = current_app.config['ALLOWED_MIMES']
    url = collect.img_url_from_hash(_hash)
    _hash = _hash[:250]
    do_404 = False
    _send_file = ''
    # we don't want to download just any file
    # - it has to be in the database
    img = sql.get_media(url=url)
    if img and _format in formats:
        width, height = formats[_format]
        format_dir = os.path.join(current_app.config['IMAGE_DIR'],_format)
        u.ensure_path(format_dir)
        download_dir = os.path.join(current_app.config['IMAGE_DIR'],'downloads')
        u.ensure_path(download_dir)

        downloaded_file = os.path.join(download_dir,_hash)

        if os.path.isfile(downloaded_file):
            # the file has already been downloaded
            _send_file = downloaded_file
        elif (download_image(url, downloaded_file, _hash)):
            # attempt to download the file
            _send_file = downloaded_file
        else:
            do_404 = True

        if len(_send_file):
            #get mime_type and create derivative image
            if u.check_image_allowed(_send_file):
                try:
                    with Image.open(_send_file) as im:
                        _mimetype = im.get_format_mimetype()
                        file_ext = _mimetype.split('/')[1]
                        if file_ext == 'x-png':
                            file_ext = 'png'
                        dest_file = os.path.join(format_dir, _hash + '.' + file_ext)
                        if os.path.isfile(dest_file):
                            logger.info('%s - dest_file found in cache',dest_file)
                        elif (u.format_image(im, dest_file, _format) == True):
                            logger.info('%s - dest_file was created',dest_file)
                        else:
                            do_404 = True
                except:
                    do_404 = True
            else:
                logger.warning('%s = unsupported mimetype',_send_file)
                do_404 = True
    else:
       do_404 = True

    if not do_404 and len(dest_file) and _mimetype:
       return send_file(dest_file, mimetype = _mimetype)
    else:
        do_404 = True

    if do_404:
        abort (404)

    return [_format,url]


def download_image(_url, local_file_path, _hash):
    rv = False
    if not is_downloadable(_url, _hash):
        return False
    else:
        try:
            r = requests.get(_url, allow_redirects=True, headers=request_headers)
            open(local_file_path, 'wb').write(r.content)
            return True
        except Exception as e:
            is_downloadable(_url,_hash, mark_as_bad=True)
            return False


def is_downloadable(url, _hash, use_cache=True, mark_as_bad=False):
    if url[0:4].lower() != 'http':
        # only download via http[s]
        return False

    if use_cache:
        # if we tried to download before don't try again
        not_downloadable_dir = os.path.join(current_app.config['IMAGE_DIR'],'not_downloadable')
        u.ensure_path(not_downloadable_dir)
        not_downloadable_file = os.path.join(not_downloadable_dir,_hash)

        if mark_as_bad:
            Path(not_downloadable_file).touch()

        if os.path.isfile(not_downloadable_file):
            return False

    """
    Does the url contain a downloadable resource
    """
    try:
        h = requests.head(url, allow_redirects=True, headers=request_headers)
    except:
        Path(not_downloadable_file).touch()
        return False

    header = h.headers
    content_type = header.get('content-type')
    try:
        if not content_type:
            Path(not_downloadable_file).touch()
            return False
        if 'text' in content_type.lower():
            Path(not_downloadable_file).touch()
            return False
        if 'html' in content_type.lower():
            Path(not_downloadable_file).touch()
            return False
    except:
        return False
    return True
