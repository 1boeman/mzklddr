'use strict'

const path = require('path')
const miniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const autoprefixer = require('autoprefixer')
const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');

module.exports = {
  watch: true,
//  mode: 'development',
  mode: 'production',
 
  entry: './src/js/main.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist_v1')
  },

  plugins: [
    new miniCssExtractPlugin(),
    new Dotenv()
  ],
  module: {
    rules: [
        {
            mimetype: 'image/svg+xml',
            scheme: 'data',
            type: 'asset/resource',
            generator: {
                filename: 'icons/[hash].svg'
            }
      },
      {
        // If you enable `experiments.css` or `experiments.futureDefaults`, please uncomment line below
        // type: "javascript/auto",
        test: /\.(sa|sc|c)ss$/,
        use: [
          {loader : miniCssExtractPlugin.loader },
          {loader : "css-loader"},
          {loader : "postcss-loader",
            options: {
                postcssOptions: {
                    plugins: [
                        autoprefixer
                    ]
                }
            }
          },
          {loader: "sass-loader"},
        ],
      },
    ],
  },

/*
{
    rules: [
      {
        test: /\.(scss)$/,
        use: [
            {
                loader: miniCssExtractPlugin.loader
            },
            {
            // Interprets `@import` and `url()` like `import/require()` and will resolve them
                loader: 'css-loader'
            },
            {
                // Loader for webpack to process CSS with PostCSS
                loader: 'postcss-loader',
                options: {
                    postcssOptions: {
                        plugins: [
                            autoprefixer
                        ]
                    }
                }
            },
            {
                // Loads a SASS/SCSS file and compiles it to CSS
                loader: 'sass-loader'
            }
        ]
      }
    ]
  },
  */
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin(),
      // For webpack@5 you can use the `...` syntax to extend existing minimizers (i.e. `terser-webpack-plugin`), uncomment the next line
      //`...`,
     // new CssMinimizerPlugin(),
    ],
  }
}
