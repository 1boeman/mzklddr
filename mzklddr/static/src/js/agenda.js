import u from './help/util'
import { load_images } from './shared'


export const agenda = () => {
    const getCurrentFilters = function(){
        const path = window.location.pathname.split('muziek/')[1];
        const pathArray = path.split('/');
        const dateFilter = window.location.href.match(/agenda\-[0-9]+\.html/);
        let locationFilter = pathArray[0].match("^[0-9]+-") ? pathArray[0] : 0;
        if (!locationFilter) locationFilter = pathArray[0].match(/regio-/) ? pathArray[0] : 0;
        return { dateFilter, locationFilter }
    }


    u.clickHandlers({
        'get_gig':function(){
            const parent_row = u.parents(this,'.gig')[0]; 
            if (!parent_row.classList.contains('opened')){
                parent_row.classList.add('opened')
            }
            if (!parent_row.classList.contains('loaded')){
                const gig_url = parent_row.querySelector('a')['href'];
                const detail = parent_row.querySelector('.detail_container');
                const closeit = () => {
                    parent_row.classList.remove('opened')
                }
                (async function(url){
                    try {
                        const resp = await fetch(url);
                        if (!resp.ok) {
                            throw new Error(`${resp.status} ${resp.statusText}`);
                        }
                        const html = await resp.text();
                        detail.innerHTML = html;
                        parent_row.classList.add('loaded')
                        load_images('detail',detail)
                        u.clck(detail.querySelector('.headline a'),closeit);
                        u.clck(detail.querySelector('.agenda-item-close'),closeit);
                    } catch (error){
                        console.log(error);
                    }
                }(gig_url + '?jx=1'));
            }
        },
        'clearFilters':function(e){
            let newPathArray = [ML.language,'muziek'];
            const f = getCurrentFilters()
            if (this.id == 'filter_day_button'){
                if (f.locationFilter) newPathArray.push(f.locationFilter);
            } else {
                if (f.dateFilter) newPathArray.push(f.dateFilter[0]);
            }
            location.href = '/' + newPathArray.join('/');
        }
    });

    load_images('thumb')

    u.q('#agenda_filters select').forEach(el => {
        u.listen(el,'change',function(e){
            const f = getCurrentFilters()
            let newPathArray = [ML.language, 'muziek'];
            switch(el.id){
                case "filter_group":
                    if (el.value != '0'){
                        newPathArray.push('regio-' + el.value);
                    }
                    if (f.dateFilter) newPathArray.push(f.dateFilter[0]);
                    break;
                case "filter_city":
                    if (el.value != '0'){
                        newPathArray.push(el.value + '-' + el.selectedOptions[0].innerText);
                    }
                    if (f.dateFilter) newPathArray.push(f.dateFilter[0]);
                    break;
                case "filter_date":
                    if (el.value > 0){
                        let dayFilter = 'agenda-' + el.value + '.html'
                        if (f.locationFilter) newPathArray.push(f.locationFilter);
                        newPathArray.push(dayFilter);
                    }
                    break;
                default:
                    console.log(el)
            }
           // window.location = 
           window.location.assign( ML['web_root']+'/'+newPathArray.join('/'))
        })
    })
}
