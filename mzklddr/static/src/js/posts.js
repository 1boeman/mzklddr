import u from './help/util'
import { cache_bust_posts } from './shared'


export const postsClickHandlers = {
    "post_remove": async (e)=>{ 
        if (confirm(ML['deletePostWarning'])){
            const resp=await fetch(e.target.dataset.href, {method:"POST",body:{}});
            const result = await resp.json()
            if (result["result"]=="1"){
                alert(ML['deletedMessage']);
                if (document.body.classList.contains('list_on_delete')){
                    window.location = ML['rootUrl']+'/'+ML['language']+'/tips/list' 
                } else {
                    location.reload()
                }
            } else{
                alert('an error occurred')
            }
        }
    }
}


export const posts = () => {
    if(u.q('.postForm').length){
        ML['isDirty'] = false;
        window.addEventListener("beforeunload", function (e) {
            // form logic must implement dirty_setter functionality itself
            if (!ML['isDirty']) return;
            
            var confirmationMessage = 'If you leave before saving, your changes will be lost.';

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        });
    }

    u.clickHandlers(postsClickHandlers);
    if (document.body.classList.contains('cached_posts')){
        cache_bust_posts()
    }
}
