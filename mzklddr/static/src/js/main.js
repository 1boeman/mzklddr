// Import our custom CSS
import '../scss/styles.scss'
import u from './help/util'
import { topMenu, darkMode } from './menu'
topMenu()
darkMode()
addCss('https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css')
u.bodyClassCallbacks({
    'Home':function(){
        import ("./home" ).then(({ home_main })=>{
            home_main();
        })
    },
    'User': function(){
        import ("./user" ).then(({ user_main })=>{
            user_main();
        })
    },
    'Search': function(){ 
        import ("./search" ).then(({ search })=>{
            search();
        })
     },
    'Agenda': function(){ 
        import ("./agenda" ).then(({ agenda })=>{
            agenda();
        })
     },
    'Gig': function(){
        import("./gig").then(({ gig })=>{ 
            gig();
        })
    },
    'Locaties-uitgaan': function(){
        import("./locaties").then(({ locaties_uitgaan })=>{ 
            locaties_uitgaan();
        })
    },
    'Locaties-uitgaan-stad': function(){
        import("./locaties").then(({ locaties_uitgaan_stad  })=>{ 
            locaties_uitgaan_stad();
        })
    },
    'Locaties-venue': function(){
        import("./locaties").then(({ locaties_venue  })=>{ 
            locaties_venue();
        })
    },
    'Post': function(){
         import("./posts").then(({ posts })=>{ 
            posts();
        })
    },
    'Muziekformulier':function(){
        import("./muziekformulier").then(({ muziekformulier})=>{ 
            muziekformulier();
        })
    },
    'Artistform':function(){
        import("./artistform").then(({ artistform })=>{ 
            artistform();
        })
    },
    'ContactForm':function(){
        u.clickHandlers({
            "sendContactForm":()=>{
                let _form = u.q('#contactform')[0]
                _form.action = ML['rootUrl'] + 'contact/formSubmit'
                _form.method='POST'
                _form.submit();
                document.body.classList.add('loading')
            }
        });
    }
});

function addCss(fileName) {
  var head = document.head;
  var link = document.createElement("link");
  link.type = "text/css";
  link.rel = "stylesheet";
  link.href = fileName;
  head.appendChild(link);
}

