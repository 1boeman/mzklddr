import u from './help/util'
import 'flatpickr/dist/themes/dark.css'
import 'choices.js/public/assets/styles/choices.min.css'
import flatpickr from "flatpickr";
import Choices from "choices.js"
import { Dutch } from "flatpickr/dist/l10n/nl.js"
import { Modal } from 'bootstrap'
import { initPhotoForm, uploadFieldListener } from './photoform';
import { postsClickHandlers } from "./posts"


const getFormElements = () => {
    let muzfrm = u.q('#muziekformulier')
    if (!muzfrm.length) return {};
    let venueSelect = u.q('#venueSelect')
    let checkboxVenue = u.q('#checkboxVenue');
    return {
        "muzfrm" : muzfrm[0],
        "eventTypeSelect" : u.q('#eventType')[0],
        "eventDates" : u.q('#eventDates')[0],
        "citySelect" : u.q('#citySelect')[0],
        "venueSelect" :venueSelect.length ? venueSelect[0] : null,
        "checkboxCity" : u.q('#checkboxCity')[0],
        "checkboxVenue" : checkboxVenue.length ? checkboxVenue[0] : null,
    };
}


const formState = function(){
    let f = getFormElements();
    let mb = u.q('#mainBlock')[0];
    let checkboxVenue = u.q('#checkboxVenue');

    if (f['eventTypeSelect'].value != "0"){
        mb.classList.add("step1")

        if (f['eventDates'].value.trim().length != "0"){
            mb.classList.add("step2")
            initPhotoForm()
        } else {
            mb.classList.remove("step2")
        }
    } else {
        mb.classList.remove("step1","step2")
    }

    if (u.q('#checkboxCity')[0].checked){
        mb.classList.add('city_as_text')
    } else{
        mb.classList.remove('city_as_text')
    }
    if (checkboxVenue.length && checkboxVenue[0].checked && u.q('.venueSelectContainer')[0].dataset.len > "1"){
        mb.classList.add('venue_as_text')
    } else if (checkboxVenue.length && u.q('.venueSelectContainer')[0].dataset.len == "1"){
        checkboxVenue[0].parentNode.style.display = 'none';
        checkboxVenue[0].checked = true;
        mb.classList.add('venue_as_text')
    } else {
        mb.classList.remove('venue_as_text')
    }
}


const initDateWidget = () => {
    let showMonths = screen.width > 768 ? 2 : 1;
    flatpickr(u.q('#eventDates')[0],{
        minDate: "today",
        inline:true,
        position: "above",
        dateFormat :"d-m-Y",
        showMonths: showMonths,
        locale: "nl",
        mode: "multiple"
    })
}


const ajaxifyForm = () => {
    let f = getFormElements();
    f['muzfrm'].addEventListener("submit", async (e) => {
        e.preventDefault();
        xhrSubmit(f)
    });
}


async function xhrSubmit(f){
    if (f == undefined) f = getFormElements();
    let formData = new FormData(f['muzfrm']); 
    document.body.classList.add('loading')
    try {
        const response = await fetch(ML.rootUrl+'muziekformulier/xhrsubmit',{
            method: 'POST',
            body: formData
        });
        if (response.ok) {
            const txt = await response.text();
            u.q('#formContainer')[0].innerHTML = txt
            if (muziekformulier()){
                const errorModal = u.q('#errorModal');
                if (errorModal.length){
                    let m =  new Modal(errorModal[0],{});
                    m.show()
                }
            } else if(u.q('#success_form').length) {
                u.q('#uploadContainer')[0].innerHTML = '';
                u.q('#submitContainer')[0].innerHTML = '';
                window.scrollTo(0,0);
                ML['isDirty']=false;
                u.clickHandlers(postsClickHandlers, u.q('.tip-listing')[0])
            }
        } else {
            alert('something went wrong. Please try again.')
        }
    } catch(err){
        alert('Fetch error:' + err); 
    } finally {
        document.body.classList.remove('loading')
    }
}


const initCitySelect = () => {
    let cs = u.q('#citySelect')
    if (cs.length){
        new Choices(cs[0], { position:"bottom", allowHTML:false})
    }
}


const initVenueSelect = () => {
    let cs = u.q('#citySelect')
    if (cs.length && cs[0].value !== "0") {
        let venueSelect = u.q('#venueSelect');
        if (venueSelect.length) {
            new Choices(venueSelect[0],{position:"bottom",allowHTML:false})
        }
    }
}


u.clickHandlers({
    "submitButton" : () => {
        u.q('#submitThis')[0].value = 'y';
        const p = u.q('#photoUploaded');
        if (p.length){
            u.q('#media')[0].value = p[0].dataset.file_id
        }
        xhrSubmit()
    }
});


export const muziekformulier = () => { 
    let f = getFormElements();
    if (!Object.keys(f).length){
        return false; 
    }
    for (let key in f){
        if (f[key] && key != 'muzfrm') {
            f[key].addEventListener('change',(e) => {
                e.preventDefault();
                ML['isDirty']=true;
                const src = e.target || e.srcElement;
                if (src.id == 'citySelect'){
                    xhrSubmit(f)
                }
                formState()
            });
        }
    }
    initCitySelect()
    initVenueSelect()
    initDateWidget()
    ajaxifyForm()
    formState()
    return true; 
}


