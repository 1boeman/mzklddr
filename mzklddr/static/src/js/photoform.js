import u from './help/util';


const uploadFieldListener = () => {
    const fileForm = u.q('#photoformulier');
    const photoField = u.q('#photo')
    const retryButton = u.q('#retryButton')
    const deleteButton = u.q('#deleteButton')

    if (photoField.length){ 
        photoField[0].addEventListener('change', async () => {
            try {
                document.body.classList.add('loading')
                const formData = new FormData(fileForm[0]);
                let response = await fetch(ML.rootUrl + 'muziekformulier/uploadthephotofilealready',{
                   method:'POST',
                   body: formData 
                });
                u.q('#uploadContainer')[0].innerHTML = await response.text(); 
                uploadFieldListener();
            } catch (err) {
                alert('Fetch error data:' + err); 
            } finally {
                document.body.classList.remove('loading')
            }
        });
    } else if (retryButton.length){
        retryButton[0].addEventListener('click',(e)=>{
            e.preventDefault();
            initPhotoForm(1);
        })
        deleteButton[0].addEventListener('click',(e)=>{
            e.preventDefault();
            u.q('#media')[0].value = "";
            initPhotoForm(1);
        })
    }
}


/*if exists */
async function initPhotoForm (ifExists=false){
    let frm = u.q('#photoformulier');
    let result = u.q('#photoUploaded');
    if ((!result.length && !frm.length) || ifExists){
        document.body.classList.add('loading');
        try {
            let url = 'muziekformulier/showmethephotofileuploadformalready';
            if (!ifExists && ML['edit_post_id'].length){
                url='muziekformulier/edituploadedphoto/'+ML['edit_post_id'];
            }
            let response = await fetch(ML.rootUrl+url);
            u.q('#uploadContainer')[0].innerHTML = await response.text();
            uploadFieldListener();
        } catch (err) {
            alert('Fetch error upload:' + err); // Error handling
        } finally {
            document.body.classList.remove('loading')
        }
    }
}

export {uploadFieldListener, initPhotoForm}
