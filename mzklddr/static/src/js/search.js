import u from './help/util'

export const search = () => {
    Object.entries(ML['active_filters']).forEach((entry)=>{
        const [key, value] = entry;
        showActiveFilter(key, value)
    })
}

u.clickHandlers({
    "showFilters":()=>{
        u.q('.search-filters')[0].classList.toggle('d-block');
    }
});

function showActiveFilter(key, value){
    let container  = u.q('#ActiveFilterContainer');
    let div = document.createElement('div');
    let a = document.createElement('a');
    let filterBlock = u.q('#'+key)[0];
    let activeFilterElement = u.q('a', filterBlock)[0];
    let labelText = activeFilterElement.textContent;

    div.classList.add("btn","btn-sm","btn-success","me-4","mb-4");
    a.innerHTML='<i class="bi bi-x-circle"></i>&nbsp;' + labelText;
    div.dataset.value = value;
    div.dataset.key = key;
    // delete filter
    div.addEventListener('click',(e)=>{
        let filterToDelete = e.currentTarget.dataset.value;
        let url = new URL(window.location.href)
        const params = new URLSearchParams(url.search);
        for (const p of params) {
            if (p[1].indexOf(filterToDelete) > -1){
                params.delete(p[0],p[1]);
                window.location = `${url.pathname}?${params}`
            }
        }
    })
    div.appendChild(a)
    container[0].appendChild(div)
}
