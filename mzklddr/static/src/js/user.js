import u from './help/util'


export const user_main = () => {
    let uform =  u.q('.userform');
    if (uform.length){
        uform[0].onsubmit = () => {
            document.body.classList.add('loading')
        }
    }

}

u.clickHandlers({
    "togglePW":(e)=>{
        let group = u.parents(e.target,'.password-group')[0]
        let pw_el = u.q('input',group)[0]
        if (pw_el.type == 'password'){
            group.classList.toggle('PWvisible')
            pw_el.type = "text"
        }else{
            group.classList.toggle('PWvisible')
            pw_el.type = "password"
        }
    }
});

