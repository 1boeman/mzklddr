import u from './help/util';
import { Modal } from 'bootstrap';
import { initPhotoForm, uploadFieldListener } from './photoform';
import { postsClickHandlers } from "./posts"


async function xhrSubmit(){
    let formData = new FormData(u.q('#muziekformulier_artist')[0]); 
    document.body.classList.add('loading')
    try {
        const response = await fetch(ML.rootUrl+'muziekformulier/xhrsubmit_artistform',{
            method: 'POST',
            body: formData
        });
        if (response.ok) {
            const txt = await response.text();
            u.q('#formContainer')[0].innerHTML = txt
            if (artistform()){
                const errorModal = u.q('#errorModal');
                if (errorModal.length){
                    let m = new Modal(errorModal[0],{});
                    m.show()
                }
            } else if(u.q('#success_form').length) {
                u.q('#uploadContainer')[0].innerHTML = '';
                u.q('#submitContainer')[0].innerHTML = '';
                window.scrollTo(0,0);
                ML['isDirty']=false
                u.clickHandlers(postsClickHandlers, u.q('.tip-listing')[0])
            }
        } else {
            alert('something went wrong. Please try again.')
        }
    } catch(err){
        alert('Fetch error:' + err); 
    } finally {
        document.body.classList.remove('loading')
    }
}


u.clickHandlers({
    "submitButton" : () => {
        const p = u.q('#photoUploaded');
        if (p.length){
            u.q('#media')[0].value = p[0].dataset.file_id
        }
        xhrSubmit()
    }
});


export const artistform = () => { 
    let aForms = u.q('#muziekformulier_artist')
    if (!aForms.length){
        return false; 
    }
    u.q('input', aForms[0]).forEach((inpt)=>{
        inpt.addEventListener("change", 
            (e) => {ML['isDirty']= true});
    })
    initPhotoForm();
    return true;
}
