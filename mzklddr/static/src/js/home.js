import u from './help/util'
import {cache_bust_posts} from './shared'

export const home_main = () => {
    cache_bust_posts(1)
    u.clickHandlers({
        "showAllCities":()=>{
            document.body.classList.toggle('citiesShown')
            u.q('.all-cities')[0].classList.toggle('d-none')
        }
    });
}
