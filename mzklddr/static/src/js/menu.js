import u from './help/util'
import Cookies from 'js-cookie'
import { Modal } from 'bootstrap'


const cookieOptions =  { SameSite: 'strict', expires: 7 }

export const topMenu = () => {
    u.clck(u.q('.top-menu .menu-item'), function(){
       window.location = u.q('a',this)[0]['href'];
    })

    /* user status */
    load_user_status()

    /* shrink logo on scroll  */
    let scrollpos = window.scrollY
    let scrollin = false;
    let scroll_btn = false;
    
    // scroll listeners
    const add_scroll_button = () =>{
        if (!scroll_btn){
            document.body.classList.add("showBtn")
            scroll_btn = true;
        }
    }

    const remove_scroll_button = () =>{
        if (scroll_btn){
            document.body.classList.remove("showBtn")
            scroll_btn = false;
        }
    }

    const add_class_on_scroll = () => {
        if (!scrollin){
            document.body.classList.add("scrollin")
            scrollin = true;
        }
    }
    const remove_class_on_scroll = () => {
        if (scrollin){
            document.body.classList.remove("scrollin")
            scrollin = false; 
        }
    }

    let timer = 0;
    const listener = () => {
        if (timer) clearTimeout(timer)
        timer = setTimeout(function(){
            scrollpos = window.scrollY;
            if (scrollpos >= 40) { 
                add_class_on_scroll();
            } else { 
                remove_class_on_scroll();
            }

            if (scrollpos >= 600){
                add_scroll_button();
            } else{
                remove_scroll_button();
            }
        },10);
    }
    listener()
    window.addEventListener('scroll',listener)
}


export const darkMode = () =>{
    let isDark = localStorage.getItem('darkmode');
    const darkButton = u.q('#darkModeCheck')[0];

    const turnOff = () => {
        localStorage.setItem('darkmode',0)
        document.documentElement.setAttribute('data-bs-theme', 'light')
    }

    const turnOn = () => {
        localStorage.setItem('darkmode',1)
        document.documentElement.setAttribute('data-bs-theme', 'dark')
    }

    const changeListen = function(){
        darkButton.checked ? turnOn() : turnOff();
    }

    darkButton.checked = isDark  == '1' ? true : false;

    changeListen()
    u.listen(darkButton,'change', changeListen)
}


async function load_user_status (){
    try {
        const response = await fetch(ML.rootUrl + '/' + ML.language+'/user/user_status');
        if (response.ok) {
            const txt = await response.text();
            u.q('.user_status_placeholder')[0].outerHTML = txt
            let el_user_status = u.q('#user_status_q');
            if (el_user_status.length){
                // show edit buttons
                const user_id = el_user_status[0].dataset.uid
                const edit_buttons = u.q('.post-edit-buttons')
                if (edit_buttons.length){
                    edit_buttons.forEach((elemnt)=>{
                        if (elemnt.dataset.uid == user_id){
                            elemnt.classList.remove('invisible')
                        }
                    })
                }
            }
        }
    } catch(err) {
        console.log('Fetch error:' + err);
    } finally {
        u.q('.top-menu-items')[0].classList.remove('opacity-0')
        u.clickHandlers({
            "scrollButton":function(){
                window.scrollTo({
                    top: 0,
                    behavior: 'smooth'
                })
            },
            "langlink": function(){
                let lang = this.dataset.lang;
                let cur_lang = document.documentElement.getAttribute('lang')
                let hreflang_link = u.q('link[hreflang="'+lang+'"]')
                if (hreflang_link.length){
                    location.href = hreflang_link[0].href
                } else if (cur_lang == lang) {
                    return false; 
                } else {
                    location.href = this.href;
                } 
            },
            "expandHamburger" : function(e){
                document.body.classList.toggle('hamburger-expand');
            }, 
            "logoutButton" : function(e){
                if (confirm(ML['logoutMessage'])){
                    window.location = this.href;
                }
            }
        },u.q('#navcontainer')[0]);
    }
}
