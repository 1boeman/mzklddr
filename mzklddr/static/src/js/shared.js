import u from './help/util'
import { postsClickHandlers } from './posts'


export const load_images = (_format, parentElement=false) => {
    u.q('.image-cell', parentElement).forEach(el => {
        //load thumbnails
        let img = el.querySelectorAll('img')[0];
        img.setAttribute("loading", "lazy");
        img.onload = function(){this.classList.add('loaded')}
        img.onerror = function (){
            let f = u.parents(this,'figure')[0]
            f.className = 'failed';
        }
        img.setAttribute("src", ML.rootUrl+'image/' + _format + '/'+el.dataset.img)
    })
}


export const cache_bust_posts = async (is_home=false) => {
    if (typeof ML['tip_page']=='undefined' || 
            parseFloat(ML['tip_page']) < 1){
        //only call this on homepage or page 1 of tips
        // because it retrieves the first tip page without cache

        const tip_listing = u.q('.tip-listing')[0]
        const post_list = u.q('.tip-listing article.post')
        if (post_list.length){
            const response = await fetch(ML.rootUrl+ 'tips/lastids')
            const last_ids = await response.json();
            if (last_ids.length){
                const last_ids_in_db = last_ids.map((item) => {
                    return item['post_id'].toString()
                })
                const last_ids_on_page = post_list.map((el) => {
                    return el.id.replace("post_","")
                })
                if (last_ids_in_db.join() != last_ids_on_page.join()) {
                    tip_listing.classList.add('loading') 
                    const context = is_home ? "1" : "";
                    const resp = await fetch(ML.rootUrl + 'tips/list_latest/' + context)
                    const html = await resp.text()
                    tip_listing.innerHTML = html;
                    tip_listing.classList.remove('loading') 
                }
            }
        }
    }
}
