import u from './help/util'
import 'choices.js/public/assets/styles/choices.min.css'
import Choices from "choices.js"
import { MarkerClusterer } from "@googlemaps/markerclusterer";


async function loadMaps() {
    (g=>{var h,a,k,p="The Google Maps JavaScript API",c="google",l="importLibrary",q="__ib__",m=document,b=window;b=b[c]||(b[c]={});var d=b.maps||(b.maps={}),r=new Set,e=new URLSearchParams,u=()=>h||(h=new Promise(async(f,n)=>{await (a=m.createElement("script"));e.set("libraries",[...r]+"");for(k in g)e.set(k.replace(/[A-Z]/g,t=>"_"+t[0].toLowerCase()),g[k]);e.set("callback",c+".maps."+q);a.src=`https://maps.${c}apis.com/maps/api/js?`+e;d[q]=f;a.onerror=()=>h=n(Error(p+" could not load."));a.nonce=m.querySelector("script[nonce]")?.nonce||"";m.head.append(a)}));d[l]?console.warn(p+" only loads once. Ignoring:",g):d[l]=(f,...n)=>r.add(f)&&u().then(()=>d[l](f,...n))})({
        key: process.env.GM_API_KEY,
        v: "weekly",
    });
}


export const locaties_uitgaan = () => {
    const openCityFromHTMLList = (city_id) => {
        let li = u.q('#l_' + city_id)
        if (li.length){
            window.location = u.q('a',li[0])[0]['href'];
        }
    }
    
    let cs = u.q("#city_select")
    if (cs.length){
        const ch = new Choices(cs[0],{position:"bottom", allowHTML:false})
        ch.passedElement.element.addEventListener('choice',function(e){
            let city_id = e.detail.choice.value;
            openCityFromHTMLList(city_id);
        })
    }

    async function initCountryMap() {
        await loadMaps()
        const { Map } = await google.maps.importLibrary("maps");
        const { AdvancedMarkerElement, PinElement } = await google.maps.importLibrary("marker");


        const map = new Map(document.getElementById("map"), {
            center: { lat: 52.371807, lng: 5.1215634},
            zoom: 7,
            mapId: "overview"
        });

        const markers = ML['cities'].map((item) => {
            const geo = item['city_geo'].split(',')
            const position =  {lat: parseFloat(geo[0]), lng: parseFloat(geo[1])};
            const pinGlyph = new google.maps.marker.PinElement({
                glyph: item['city_name'].substring(0,3),
                glyphColor: "white"
            });
            const marker = new google.maps.marker.AdvancedMarkerElement({
                position,
                title: item['city_name'],
                content: pinGlyph.element,
            });

            marker.addListener("click", () => {
                openCityFromHTMLList(item['city_id'])
            });

            return marker 
        });

        new MarkerClusterer({ markers, map });
    }
    initCountryMap();
}



async function initCityMap(mapContainer, map_geo, venue_rows, zoom, map_id){
    await loadMaps()
    const { Map } = await google.maps.importLibrary("maps");
    const { AdvancedMarkerElement, PinElement } = await google.maps.importLibrary("marker");

    const geo = map_geo.split(',');
    const map = new Map(mapContainer, {
        center: { lat: parseFloat(geo[0]), lng:parseFloat(geo[1]) },
        zoom:zoom,
        mapId: map_id
    });

    const infowindow = new google.maps.InfoWindow({
        content: "",
    });

    const markers = venue_rows.filter((item) => {
        if (!item["address_geo"]) {
            return false; 
        }
        return true;
    }).map((item) => {
        const geo = item['address_geo'].split(',')
        const position =  {lat: parseFloat(geo[0]), lng: parseFloat(geo[1])};
        const marker = new google.maps.marker.AdvancedMarkerElement({
            position,
            title: item['venue_title'],
        });

        marker.addListener("click", () => {
            let content = ['<div class="window-info"><strong>']

            content.push('<a data-id="' + item['venue_id'] 
                + '" onclick="ML[\'handleClick\'](this.dataset.id)">')
            if (item['venue_address_title']){
                content.push( item['venue_address_title'])
            } else {
                content.push( item['venue_title'])
            }
            content.push( '</strong><br>' )
            content.push( item['street_name'] + ' ' + item['number'])
            content.push( '<br>' )
            content.push(item['city_name'])
            
            content.push('</div>')
            infowindow.setContent(content.join(' '))
            infowindow.open({
                anchor: marker,
                map,
            })
        });

        return marker 
    });

    new MarkerClusterer({ markers, map });
}


export const locaties_uitgaan_stad = () => {
    ML['handleClick'] = (venue_id) => {
        window.location = document.getElementById(venue_id)['href']
    }

    initCityMap(document.getElementById("map"),ML['city']['city_geo'], ML['venue_rows'],12,"overview");

    u.clickHandlers({
        "followChildLink":function(e){
            let a = u.q('a', this)[0];
            window.location = a['href'];
        }
    });
}


export const locaties_venue = () => {
    ML['venue_addresses'].forEach((va)=>{
        let query;
        if (va['street_name'] != 'onbekend'){
            query = encodeURI([
                va['street_name'],
                va['number'],
                va['city_name'],
                va['country_name']].join(' '));
        } else {
            query = encodeURI([
                va['city_name'],
                va['country_name']].join(' '));
        }   
        let html = '<iframe frameborder="0"'
            +' style="width:100%; height:300px" src="'
            +'https://www.google.com/maps/embed/v1/place?key='
            + process.env.GM_API_KEY
            +'&q=' + query
            +'"></iframe>';
        u.q('#address_' + va['address_id'])[0].innerHTML=html
      //  initCityMap(document.getElementById("address_" + va['address_id']), va['address_geo'], [va], 18, "overview_" + va['address_id']);
    })
}
