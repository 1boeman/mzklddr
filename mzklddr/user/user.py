import os
import re
import hashlib
import logging
import glob
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash
from flask import Blueprint, render_template, g, abort, current_app, request, url_for, redirect, session
from mzklddr.models.user import User
from flask_login import current_user, login_user, logout_user, login_required
from flask_babel import format_datetime,_,lazy_gettext
from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, HiddenField, PasswordField, EmailField
from wtforms.validators import DataRequired, InputRequired, ValidationError, Optional, Email, Length
import time
from mzklddr.helpers import sql
from mzklddr.helpers import collect
from mzklddr.helpers import util as u

bp=Blueprint('user', __name__)

PASS_MIN_LENGTH = 12
PASS_MAX_LENGTH = 120

USER_MIN_LENGTH = 3
USER_MAX_LENGTH = 30


class LoginForm(FlaskForm):
    email = StringField(lazy_gettext('Email'), validators=[InputRequired()])
    passwd = PasswordField(lazy_gettext('Password'), validators=[InputRequired()])


class RegisterForm(FlaskForm):
    email = EmailField(lazy_gettext('Email address'), [DataRequired(), Email()])


class SignUpForm(FlaskForm):
    passwd1 = PasswordField(lazy_gettext('Password'), validators=[InputRequired(), Length(min=PASS_MIN_LENGTH, max=PASS_MAX_LENGTH)])
    passwd2 = PasswordField(lazy_gettext('Repeat password'), validators=[InputRequired(), Length(min=PASS_MIN_LENGTH,max=PASS_MAX_LENGTH )])


class ResetForm(FlaskForm):
    email = EmailField(lazy_gettext('Email address'), [DataRequired(), Email()])


class ResetPasswordForm(FlaskForm):
    use_mfa = BooleanField(lazy_gettext('Use two factor authentication for extra security?'), default=False) 
    passwd1 = PasswordField(lazy_gettext('Password'), validators=[InputRequired(), Length(min=PASS_MIN_LENGTH,max=PASS_MAX_LENGTH)])
    passwd2 = PasswordField(lazy_gettext('Repeat password'), validators=[InputRequired(), Length(min=PASS_MIN_LENGTH,max=PASS_MAX_LENGTH)])


class UserNameForm(FlaskForm):
    user_name = StringField(lazy_gettext('User name'), validators=[InputRequired(),Length(min=USER_MIN_LENGTH, max=USER_MAX_LENGTH)])


def encrypt_string(_string):
     return hashlib.sha256(_string.encode('utf-8')).hexdigest()


def make_unique_url(_type, file_data_list, email):
    # create_tmp_user_file to match url
    prefix = encrypt_string(email)
    user_tmp = u.get_user_tmp()
    unique_id1 = u.uniqid()
    unique_id2 = u.uniqid()
    ts = time.time()
    file_name = '_'.join([prefix, unique_id1,  unique_id2, _type])
    # store unique_id in session as well
    session[_type] = file_name
    user_tmp_file = os.path.join(user_tmp, file_name)
    with open(user_tmp_file, "w") as _file:
        _file.write(str(ts))
        _file.write('\n')
        for i in file_data_list:
            _file.write(str(i) + '\n' )
    _url = url_for(_type, unique_id=file_name, _external=True, _scheme='https')
    return _url


def open_tmp_file(unique_id) -> list:
    lines =[]
    user_tmp = u.get_user_tmp()
    user_tmp_file = os.path.join(user_tmp, unique_id)
    if os.path.isfile(user_tmp_file):
        with open(user_tmp_file, "r") as _file:
            lines = _file.readlines()
    return lines 


def check_tmp_file_age(unique_id, max_seconds):
    """ Check age of tmp file
        returns False if file is older than max_seconds(and thus expired)
    """
    user_tmp = u.get_user_tmp()
    user_tmp_file = os.path.join(user_tmp, unique_id)
    if os.path.isfile(user_tmp_file):
        ts = time.time()
        file_age =  ts - os.path.getmtime(user_tmp_file)
        if file_age < max_seconds:
            return True
    return False


def delete_tmp_file(unique_id):
    user_tmp = u.get_user_tmp()
    user_tmp_file = os.path.join(user_tmp, unique_id)
    if os.path.isfile(user_tmp_file):
        os.remove(user_tmp_file) 


def mail_timer_path(_type, email):
    file_name = encrypt_string(email) + '_' + _type
    user_tmp = u.get_user_tmp()
    return os.path.join(user_tmp, file_name)


def create_mail_timer_file(_type, email, content=None):
    # create_tmp_user_file to match url
    user_tmp_file = mail_timer_path(_type, email)
    with open(user_tmp_file, "w") as _file:
        if content:
            _file.write(f"{content}")
        else:
            _file.write(email)


def read_mail_timer_file(_type, email):
    lines=[]
    user_tmp_file = mail_timer_path(_type, email)
    if os.path.isfile(user_tmp_file):
        with open(user_tmp_file, "r") as _file:
            lines = _file.readlines()
    return lines 


def delete_mail_timer_file(_type, email):
    user_tmp_file = mail_timer_path(_type, email)
    if os.path.isfile(user_tmp_file):
        os.remove(user_tmp_file) 


def check_mail_timer_file(_type, email, max_seconds):
    """ Check age of timer file
        returns False if file is older than max_seconds
    """
    user_tmp_file = mail_timer_path(_type, email)
    if os.path.isfile(user_tmp_file):
        ts = time.time()
        file_age =  ts - os.path.getmtime(user_tmp_file)
        if file_age < max_seconds:
            return True
    return False


def password_complexity_check(passwd1):
    ok = True
    if passwd1.islower() or passwd1.isupper():
        ok = False
    elif passwd1.isdigit():
        ok = False
    elif passwd1.isalpha():
        ok = False
    elif passwd1.isalnum():
        ok = False
    return ok

errmsg1 = _('Please use at least one uppercase letter, one lowercase letter, one number, and one symbol (like @~#!%* etc.).') 

def my_login_user(user_id):
    if not current_user.is_authenticated:
        logger = current_app.logger
        user = User(user_id=user_id)
        login_user(user)
        logger.info(f"user {user_id} logged in ")
        user.update_last_login() 
        delete_mail_timer_file('login_failed', user.email) 
        return user
    return False 


@bp.route('/user_name', methods=['GET', 'POST'])
def user_name_edit():
    logger = current_app.logger
    errmsg1 = _('Sorry, the user name you chose is already in use.')
    errmsg2 = _('New name is the same as old name.')
 
    if not current_user.is_authenticated:
        abort(403)
    
    user_id = current_user.user_id
    tpl_data = {"user_id":user_id}
    user = None
    form = UserNameForm(data={"user_name":current_user.user_name})

    if form.validate_on_submit():
        new_name = form.user_name.data.strip()
        user = User(current_user.user_id)
        if user.user_name == new_name:
            form.user_name.errors.append(errmsg2)
        else: 
            user_exists = sql.get_user(user_name = new_name)
            if user_exists:
                form.user_name.errors.append(errmsg1)
            else:
                user.user_name = new_name
                user.save()
                logger.info('user_name update for user ' + str(user_id))
                tpl_data['success']=True
        tpl_data['user_name'] = new_name
    return render_template('user_user_name.html',form=form, 
                            data=tpl_data)


@bp.route('/reset/<unique_id>', methods=['GET', 'POST'])
def reset_password(unique_id):
    """
    this is the actual password reset page
     (used for both logged in and anonymous user)
    ## if user is logged in also shows 2fa option
    """
    logger = current_app.logger
    user = None
    usr_nav_link=2
    tpl_data = {
        "unique_id": unique_id,
        "file_not_found": False,
        "success": False,
        "failed": False,
        "please_wait": False,
        "user_not_present": False,
    }
    if current_user.is_authenticated:
        # logged in user
        if unique_id == 'logged_in':
            usr_nav_link=6
            user=User(current_user.user_id)
            obj = {}
            obj['use_mfa'] = len(user.authentication)
            form = ResetPasswordForm(data=obj)
        else:
            abort(404)
    else:
        # not logged in but following a forgot pw link
        lines = open_tmp_file(unique_id)
        if not len(lines):
            tpl_data['file_not_found'] = True
        else:
            user = User(email=lines[1].strip())

        if not user or not user.id:
            tpl_data['user_not_present'] = True

        form = ResetPasswordForm()

    if form.validate_on_submit():
        if not password_complexity_check(form.passwd1.data):
            form.passwd1.errors.append(errmsg1)
        elif form.passwd1.data != form.passwd2.data:
            form.passwd2.errors.append(_('Passwords do not match. Please make sure the two password fields match.'))
        else:
            user.password = form.passwd1.data
            user.save()
            if current_user.is_authenticated:
                user.authentication = form.use_mfa.data
            else:        
                my_login_user(user.user_id)
            tpl_data['success']=True

    return render_template('user_reset_password.html',form=form, 
                            data=tpl_data, 
                            usr_nav_link=usr_nav_link)


@bp.route('/password_reset', methods=['GET', 'POST'])
def password_reset():
    """ mail a password reset link"""
    MAIL_WAIT_TIME = current_app.config['MAIL_WAIT_TIME']
    logger = current_app.logger
    tpl_data = {}
    m_data = {}
    success = False
    failed = False
    please_wait = False
    user_not_present = False
    form = ResetForm()
    if form.validate_on_submit():
        form_mail = form.email.data.strip()
        tpl_data['form_mail'] = form_mail
        # prevent hammering of mailserver
        if not check_mail_timer_file('password_reset', form_mail, MAIL_WAIT_TIME):
            user_present = sql.get_user(email=form_mail)
            if user_present:
                # prepare mail fields
                m_data['user_name'] = user_present['user_name']
                m_data['site_name'] = current_app.config['SITE_NAME']
                m_subject = _('Account on ') + ' ' + current_app.config['SITE_NAME']
                m_data['one_time_login_url'] = make_unique_url('user.reset_password', [form_mail], form_mail)
                m_body = render_template('email_password_reset.html', data=m_data)
                mail_sent = False
                mail_sent = u.send_email(form_mail, m_subject, m_body)
                if mail_sent:
                    success=True
                    create_mail_timer_file('password_reset', form_mail)
                else:
                    failed=True
                    logger.error(f"couldn't send reset mail {form_mail}")
            else:
                user_not_present = True
        else:
            please_wait=True

    return render_template('user_reset.html',form=form, 
                            user_not_present = user_not_present,
                            success=success,
                            failed=failed,
                            please_wait = please_wait,
                            data = tpl_data, 
                            usr_nav_link=2)


@bp.route('/<user_id>')
@bp.route('/<user_id>/<page>')
def user_profile(user_id, page=0):
    user = User(user_id=user_id)
    if not user.user_id:
        abort(404)
    else:
        tpl_data = collect.post_listing(user_id=user_id, page=page)
        tpl_data['user'] = user
        tpl_data['list_mode'] = 'user_profile'
        tpl_data['user_id'] = user_id
        #breadcrumbs = u.crumb(url_for('user.register'), _('Profiles'), [])
        #u.crumb(url_for('user.user_profile', user_id=user_id), user.user_name, breadcrumbs)
        return render_template('user_profile.html',data=tpl_data, usr_nav_link=4)


@bp.route('/register', methods=['GET', 'POST'])
def register():
    MAIL_WAIT_TIME = current_app.config['MAIL_WAIT_TIME']
    logger = current_app.logger
    form = RegisterForm()
    success = False
    failed = False
    please_wait = False
    if form.validate_on_submit():
        # check if email is already present
        form_mail = form.email.data.strip()
        m_data = {}
        # prevent hammering of mailserver
        if not check_mail_timer_file('register',form_mail, MAIL_WAIT_TIME):
            # prepare mail fields
            m_data['user_name'] = form_mail.split('@')[0]
            m_data['site_name'] = current_app.config['SITE_NAME']
            m_subject = _('Account on ') + ' ' + current_app.config['SITE_NAME']
            # check if user is already registered
            present_user = sql.get_user(email=form_mail)

            if not present_user:
                m_data['one_time_login_url'] = make_unique_url('user.signup', [form_mail], form_mail)
                m_body = render_template('email_register.html', data=m_data)
            else:
                m_body = render_template('email_register_reset.html', data=m_data)
            mail_sent = False
            mail_sent = u.send_email(form_mail, m_subject, m_body)
            if mail_sent:
                success=True
                create_mail_timer_file('register',form_mail)
            else:
                failed=True
                logger.error(f"could not send register mail {form_mail}")
        else:
            please_wait=True
    return render_template('user_register.html', form=form, 
                            failed=failed, 
                            success=success, 
                            please_wait = please_wait, 
                            usr_nav_link=0)


@bp.route('/signup/<unique_id>', methods=['GET', 'POST'])
def signup(unique_id):
    # checks emailed register link and registers account
    logger = current_app.logger
    form = SignUpForm()
    tpl_data = {
        "success" : False,
        "file_not_found" : False,
        "email_taken" : False,
    }

    lines = open_tmp_file(unique_id)
    if not len(lines):
        tpl_data['file_not_found'] = True
    else:
        _email = lines[1].strip()
        possible_user_name = _email.split('@')[0]
        user_row = sql.get_user(email=_email)
        if user_row:
            tpl_data['email_taken'] = True

        if form.validate_on_submit() and not user_row:
            passwd1 = form.passwd1.data
            passwd2 = form.passwd2.data
            if not password_complexity_check(passwd1):
                form.passwd1.errors.append(errmsg1)
            elif passwd2 != passwd1:
                form.passwd2.errors.append(_('Passwords do not match. Please make sure the two password fields match.'))
 
            else:
                result = None
                try:
                    # create new user
                    pw_hash = generate_password_hash(passwd1)
                    result = sql.save_user(
                                user_name = unique_id,
                                email = _email,
                                password = pw_hash,
                                active = 1,)
                except Exception as e:
                    logger.exception(e)

                if result:
                    # user was created in db
                    tpl_data['success']=True
                    tpl_data['user_id']=result
                    try:
                        # try to set username to something more sensible
                        sql.save_user(
                            user_id = result,
                            user_name = possible_user_name + '__' +str(result),
                        )
                    except Exception as e:
                        # if this fails it's too bad, but user can login.
                        logger.exception(e)

                    logger.debug('created new user for ' + _email + ' with id '+ str(result))
                    user = User(user_id=result)
                    login_user(user)
                    logger.debug('logged in user with id '+ str(result))
     
                    tpl_data['user_name'] = user.user_name
                else:
                    form.passwd2.errors.append(_('Sorry could not create   account due to unknown error. Please try again later.'))
                    logger.error('could not create  for ' + _email + ' with')
    return render_template('user_signup.html', form=form, data=tpl_data, unique_id=unique_id, usr_nav_link=0)


@bp.route('/logon/<unique_id>', methods=['GET'])
def logon(unique_id):
    """ login via email link for mfa """
    logger = current_app.logger
    tpl_data = {
        "success" : False,
        "file_not_found" : False,
    }

    lines = open_tmp_file(unique_id)
    if current_user.is_authenticated:
        tpl_data['success']=True
        return render_template('user_logon.html',
                            data=tpl_data,
                            usr_nav_link=1)
    elif 'user.logon' not in session or session['user.logon'] != unique_id:
        tpl_data['unique_id_not_in_session'] = True
    elif not len(lines):
        tpl_data['file_not_found'] = True
    elif check_tmp_file_age(unique_id, current_app.config['LOGIN_VALID_DURATION']):
        uid=int(lines[1])
        user = my_login_user(uid)
        delete_tmp_file(unique_id)
        delete_mail_timer_file('loginmail', user.email)
    else:
        uid=int(lines[1])
        user = User(user_id=uid)
        tpl_data['file_expired'] = True
        delete_tmp_file(unique_id)
        delete_mail_timer_file('loginmail', user.email)
        logger.debug(f"expired {unique_id} logon file accessed and removed ")
    return render_template('user_logon.html',
                            data=tpl_data,
                            usr_nav_link=1)


@bp.route('/login', methods=['GET', 'POST'])
def login():
    """ login on correct password, 
        or in case of activated mfa
        send a link to login with via email 
    """
    MAIL_WAIT_TIME = current_app.config['MAIL_WAIT_TIME']
    logger = current_app.logger
    user_name = None
    mfa = False
    success_mail = False
    repeated=False
    failed_mail = False
    login_failed = False
    blocked = False
    if current_user.is_authenticated:
        user_name = current_user.user_name
    
    logger = current_app.logger
    form = LoginForm()
    attempts = 1
    max_attempts = 8
    if form.validate_on_submit():
        form_mail = form.email.data
        lines = read_mail_timer_file('login_failed', form_mail)
        #check the age of the failed login file
        active_check_file=check_mail_timer_file('login_failed',form_mail,
                             current_app.config['LOGIN_BLOCK_DURATION']) 
        if len(lines) and active_check_file:
            attempts += int(lines[0])
            if attempts > max_attempts:
                logger.warning(f"blocked {form_mail} login after {attempts} attempts")
                blocked = True
        elif not active_check_file:
            delete_mail_timer_file('login_failed', form_mail) 
            attempts = 1
            logger.debug(f"removed login_failed timer file for {form_mail} due to expiration")

        user_row = sql.get_user(email=form_mail)
        if not blocked and user_row:
            password = form.passwd.data
            user = User(user_row['user_id'])
            mfa = len(user.authentication)
            passed=False
            try:
                passed = user.check_password(password)
            except Exception as e:
                logger.debug("check password failed - now test for Drupal pw")
                logger.exception(e)

            if passed or user.check_drupal_password(password):
                if not mfa:
                    # logon user directly
                    my_login_user(user_row['user_id'])
                    user_name = current_user.user_name
                else:
                    #send mail for mfa
                    if not check_mail_timer_file('loginmail', form_mail, MAIL_WAIT_TIME):
                        # send logon mail for mfa
                        m_data = {}
                        m_subject = current_app.config['SITE_NAME'] + ' ' + _('Log in')
                        m_data['site_name'] = current_app.config['SITE_NAME']
                        m_data['one_time_login_url'] = make_unique_url('user.logon', [user.user_id], user.email)
                        m_body = render_template('email_logon.html', data=m_data)
                        mail_sent = False
                        mail_sent = u.send_email(user.email, m_subject, m_body)
                        if mail_sent:
                            success_mail=True
                            create_mail_timer_file('loginmail', form_mail)
                            logger.debug(f"sent email login link to {user.email}")
                        else:
                            failed_mail=True
                    else:
                        success_mail=True
                        repeated=True
            else:
               # wrong password
               create_mail_timer_file('login_failed', form_mail, content=str(attempts))
               login_failed=True
        else:
            # wrong email
            create_mail_timer_file('login_failed', form_mail, content=str(attempts))
            login_failed=True
    if login_failed:
        logger.info(f"Failed login for email {form_mail}")

    return render_template('user_login.html', form=form,
                            user_name=user_name,
                            blocked=blocked,
                            attempts = attempts,
                            max_attempts = max_attempts,
                            login_failed=login_failed,
                            success_mail=success_mail,
                            repeated=repeated,
                            failed_mail=failed_mail,
                            usr_nav_link=1)


@bp.route('/log_out')
@login_required
def log_out():
    logout_user()
    return redirect(url_for('user.login'))


@bp.route('/user_status')
def user_status():
    return render_template('user_status.html')


@bp.route('/user_settings')
@login_required
def user_settings():
    pass
