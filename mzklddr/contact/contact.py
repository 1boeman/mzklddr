from flask import Blueprint, render_template, url_for, request, session, redirect, current_app
from mzklddr.models.post import Post
from flask_wtf import FlaskForm
from flask_babel import lazy_gettext, _
from string import Template
from mzklddr.helpers import util as u
from mzklddr.helpers.captcha import captcha
from wtforms import StringField, TextAreaField, HiddenField
from wtforms.validators import DataRequired, InputRequired, ValidationError, Email


bp = Blueprint('contact', __name__)

class ContactForm(FlaskForm):
    sender_name = StringField(lazy_gettext('Your name'), validators=[InputRequired(lazy_gettext('Please provide your name'))])
    email = StringField(lazy_gettext('Your email address'), validators=[InputRequired(lazy_gettext('Please provide your email so we can reply to your message')), Email(lazy_gettext("This field requires a valid email address"))])
    captcha = StringField(lazy_gettext('Please repeat the text in the image'), validators=[InputRequired(lazy_gettext('Please repeat the text in the image'))])
    messageText = TextAreaField(lazy_gettext("Message"),validators=[InputRequired(lazy_gettext('No message or question was specified'))])


@bp.route('/')
def contact():
    session['just_did_this'] = False
    form = ContactForm(formdata=None)
    crumbs = u.crumb(url_for('contact.contact'), _('Contact'))
    data = {}
    return render_template(
        'contact.html',
        data=data,
        breadcrumbs=crumbs,
        success=False,
        form=form)


@bp.route('/formSubmit', methods=['POST'])
def contact_form_submit():
    if session['just_did_this']:
        # prevent resending by back button
        return redirect(url_for('contact.contact'))
    success=False
    form = ContactForm()  
    crumbs = u.crumb(url_for('contact.contact'), _('Contact'))
    data = {}
    if form.validate_on_submit():
        if captcha.validate(): 
            mail_content = Template("$name ($email) said:\n $message")
            mail_content = str(mail_content.safe_substitute(
                name=form.sender_name.data,
                email=form.email.data,
                message=form.messageText.data,
            ))
            if u.send_email(
                current_app.config['ADMIN_MAIL'],
                'a message from contactform @ ' + current_app.config['SITE_NAME'],
                mail_content):
                session['just_did_this'] = True
                return redirect(url_for('contact.contact_done'))
            else:
                form.email.errors.append(
                    _('Sorry, but a techical problem prevents sending your message. Please try again later..'))
        else:
            form.captcha.errors.append(_('Please repeat the text in the image'))
 
    return render_template(
        'contact.html',
        data=data,
        breadcrumbs=crumbs,
        success=success,
        form=form)


@bp.route('/formDone', methods=['GET'])
def contact_done():
    success=True
    crumbs = u.crumb(url_for('contact.contact'), _('Contact'))
    data = {}
    return render_template(
        'contact.html',
        data=data,
        breadcrumbs=crumbs,
        success=success,
        form=None)
