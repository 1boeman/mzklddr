from flask import Blueprint, render_template, g, abort, current_app, session, url_for
import click
import logging
import datetime
import requests
from mzklddr.helpers import sql
from mzklddr.helpers import collect
from mzklddr.helpers import util as u
from mzklddr.models.post import Post
from urllib.parse import quote_plus


bp=Blueprint('cmd', __name__)


@bp.cli.command('search_index_posts')
@click.option('--page', default=None, help='specify which page of posts to index')
@click.option('--post_id', default=None, help='specify which post_id to ondex')
def search_index_posts(page, post_id):
    if page:
        post_list = sql.user_post_list(page=page)
        for row in post_list:
            r = collect.search_index_post(row['post_id'])
            print (r)
    else:
        r = collect.search_index_post(post_id)
        print (r)


@bp.cli.command('new_users_report')
def report():
    mail_body_list = []
    result = sql.query("select user_id, user_name, email, last_login,user_date_created from user where user_date_created > DATE(NOW()-INTERVAL 1 DAY)")
    for row in result:
        mail_body_list.append("new user: "+str(row))

    result = sql.query("select * from post where date_created > DATE(NOW()-INTERVAL 1 DAY)")
    for row in result:
        mail_body_list.append("new post: "+str(row))
 
    if len(mail_body_list):
        mail_body = "\n\n[*] ".join(mail_body_list)
        u.send_email(current_app.config['ADMIN_MAIL'], "new user report", mail_body)


@bp.cli.command('activate_post_events')
@click.option('--post_id', default=None, help='specify which post_id to activate')
@click.option('--city_id', default=None, help='specify which city_id')
def activate(post_id, city_id):
    i=0
    logger=current_app.logger
    if post_id:
        post = sql.get_post(post_id)
        posts = [post]
    else:
        posts = sql.user_post_list(not_published=True, post_type='event')
    for e in posts:
        i+=1
        if i > 100:
            print('stopped')
            continue
        post = Post(post_id=e['post_id']) 
        try:
            post.publish(city_id_arg=city_id)
            msg = f"Published {post.post_id} to calendar"
            logger.info(msg)
            print(msg)
        except Exception as e:
            msg = f"[*] Could not publish {post.post_id}"
            logger.info(msg)
            click.echo(msg, err=True)
            logger.exception(e)


@bp.cli.command('deactivate_venue')
@click.argument('venue_id')
def deactivate_venue(venue_id):
    venue_row = sql.get_venue(venue_id)
    if venue_row:
        print (venue_row) 
        if click.confirm(f"deactivate {venue_row['venue_title']}?"):
            q = """ update venue set venue_deactivated = NOW()
                    where venue_id = %s """
            sql.save(q,(venue_id,))
            venue_row = sql.get_venue(venue_id)
            click.secho(venue_row)
    else:
        click.secho('not found')

    

@bp.cli.command('get_geo')
@click.argument('search_string')
def geocode_city(search_string):
    API_KEY = current_app.config['GM_API_KEY']
    param = quote_plus(search_string)
    url = "https://maps.googleapis.com/maps/api/geocode/json?"
    url += f"key={API_KEY}&address={param}&sensor=false"
    resp = requests.get(url)
    data = resp.json()
    print (data)


@bp.cli.command('get_nearby_places')
@click.argument('city_id')
def geocode_city(city_id):
    API_KEY = current_app.config['GM_API_KEY']
    sql.get_city(city_id)
    city_row = sql.get_city(city_id)
    print(city_row)
    city_row['city_geo']
 
    url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
    url += f"key={API_KEY}&"
    url += f"location={city_row['city_geo']}&"
    url += f"radius=10000&"
    url += f"type=postal_town"
    print(url)
    resp = requests.get(url)
    data = resp.json()
    print (data)


@bp.cli.command('geocode_city')
@click.argument('city_id')
def geocode_city(city_id):
    API_KEY = current_app.config['GM_API_KEY']
    city_row = sql.get_city(city_id)
    print (city_row)
    param = quote_plus(", ".join([city_row['city_name'], 
        city_row['group_name'], 
        city_row['country_name']])) 
    url = "https://maps.googleapis.com/maps/api/geocode/json?"
    url += f"key={API_KEY}&address={param}&sensor=false"
    resp = requests.get(url)
    data = resp.json()
    print (data)
    lat_lng = data['results'][0]['geometry']['location']
    value = ",".join([str(lat_lng['lat']),str(lat_lng['lng'])])
    print (value)
    if click.confirm(f"update geocode for {city_row['city_name']} with this value?"):
        value = ",".join([str(lat_lng['lat']),str(lat_lng['lng'])])
        sql.update_city(city_id,city_geo=value)
        city_row = sql.get_city(city_id)
        click.secho(city_row)


@bp.cli.command('group_ids')
def show_group_ids():
    rows = sql.get_city_groupings()
    for row in rows:
        click.secho(row)


@bp.cli.command('add_city')
@click.argument('name')
@click.argument('country_id')
@click.argument('group_id')
def add_city(name, country_id, group_id):
    # add city to db
    from time import time 
    city_id = int(time())
    country = sql.get_country(country_id)
    if not country:
        raise Exception(f"country_id {country_id} not found")

    grouping = sql.get_city_groupings(group_id)
    if not len(grouping):
        groupings =  sql.get_city_groupings()
        print(groupings) 
        raise Exception(f"group_id {group_id} not found")
 
    if sql.query('select * from city where city_name = %s and country_id =%s',
        (name,country_id), one=True):
        click.secho(f"{name} in {country['country_name']} already exists!")
    elif click.confirm(f"Create city {name} in country {country['country_name']}? with id {city_id}"):
        sql.save_city(city_id, name, country_id)
        print (city_id)
        city = sql.get_city(city_id)
        click.secho(f"{city['city_name']} saved with city_id {city['city_id']}")
        #add city_grouping link
        sql.save_city_groupings_has_city(group_id, city_id)

        # add diverse locaties to db
        collect.create_diverse_locaties(city_id)
        div_loc_id = collect.diverse_locaties_venue_id(city_id) 
        click.secho(f"{div_loc_id} venue_id diverse locaties created")
    # show  city_id
