from flask import (Blueprint, request, render_template, abort, current_app, url_for)
from mzklddr.helpers.solr.client import select_request,select,solr_escape,get_solr
from mzklddr.helpers import sql
from mzklddr.helpers.caching import cache
from mzklddr.helpers import collect
from mzklddr.helpers import util as u
from flask_login import current_user
from flask_babel import _
import json
import math
import re


bp = Blueprint('search', __name__)


@bp.route('/')
@cache.cached(key_prefix=u.make_key)
def find():
    logger = current_app.logger
    solr_conf = {}
    solr_parms = []
    data = {}
    respp = int(current_app.config['SEARCH_RESULTS_PER_PAGE'])
    start = 0
    end = respp
    q = request.args.get('q','*')
    p = int(request.args.get('p',0))
    fqs = request.args.getlist('fq')
    active_filters = {}

    data['pageless_request_url'] = re.sub('[&?]p=[0-9]+','',request.full_path)
    if p:
        start = int(p) * respp
        end = start + respp
    data['q'] = q
    data['p'] = p
    solr_parms.append(("q",q))
    solr_parms.append(("rows",respp))
    solr_parms.append(("start",start))
    groups = sql.get_city_groupings()
    data['city_groupings'] = dict(map(lambda x: (x['group_id'],x ), groups))
    data['start'] = start
    r = select_request(extra_params=solr_parms,config_override=solr_conf)
    if r['response'] and r['response']['numFound']:
        # numbers
        data['total_pages'] =  math.ceil(int(r['response']['numFound'])/respp)
        data['page_range'] = collect.page_range(p, data['total_pages'])
        if int(r['response']['numFound']) < end:
            end = r['response']['numFound']

        # filters
        if 'fq' in r['responseHeader']['params']:
            _fq = r['responseHeader']['params']['fq']
            if type(_fq) == str:
                _fq = [_fq]
            for f in _fq:
                b = re.split(r'[\:\}]', f)
                filter_key = b[0].replace('{!field f=','')
                active_filters[filter_key] = b[1]
    else:
        data['total_pages'] = 0
    data['end'] = end
    data['solr'] = r
    #logger.debug(r)

    crumbs = u.crumb(url_for('search.find'), _('Search'))
    return render_template('search.html',data=data, breadcrumbs=crumbs, active_filters=active_filters)


@bp.route('/suggest')
@bp.route('/')
@cache.cached(key_prefix=u.make_key)
def suggest():
    rv = {"suggestions":{}}
    terms = request.args.get('terms')
    if terms:
        solr = get_solr()
        solr = "/".join([solr,'suggest']) 
        params = {
            "suggest":"true",
            "suggest.dictionary":"mySuggester",
            "suggest.q":solr_escape(terms),
        }
        r = requests.get(solr,params)
        r = r.json()
        suggestings = r['suggest']['mySuggester']
        for term in suggestings:
            for y in suggestings[term]['suggestions']:
                rv['suggestions'][y['term']] = {"label":y["term"],"value": y["term"]}
        rv['suggestions'] = list(rv['suggestions'].values())
    return rv


