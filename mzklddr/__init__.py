import os
import time
import logging
from datetime import datetime
from logging.handlers import RotatingFileHandler
from flask import Flask, g, url_for, abort, request, render_template_string, render_template, session, send_from_directory
from flask_babel import Babel, _, format_datetime
from flask_login import LoginManager, current_user, logout_user
from mzklddr.models.user import User
from mzklddr.helpers import db
from mzklddr.helpers import collect
from mzklddr.helpers.captcha import captcha
from mzklddr.helpers.caching import cache
from mzklddr.helpers.solr import client
from mzklddr.home import home
from mzklddr.agenda import agenda
from mzklddr.search import search
from mzklddr.muziekformulier import muziekformulier
from mzklddr.gig import gig
from mzklddr.locaties import locaties
from mzklddr.uitgaan import uitgaan
from mzklddr.image import image
from mzklddr.user import user
from mzklddr.tips import tips
from mzklddr.content import content
from mzklddr.contact import contact
from mzklddr.cmd import cmd
from flask_session import Session
from cachelib.file import FileSystemCache
from flask_caching import Cache
import click
from flask.cli import with_appcontext
from werkzeug.middleware.proxy_fix import ProxyFix


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    ch = logging.StreamHandler()

    _handlers = [ch]
    file_handler = RotatingFileHandler(os.path.join(app.instance_path, 'mzklddr.log'), maxBytes=10000000, backupCount=10)
    _handlers.append(file_handler)
    logging.basicConfig(handlers=_handlers, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)


    app.config.from_mapping (
        SEND_FILE_MAX_AGE_DEFAULT=300,
        MAX_CONTENT_LENGTH=16 * 1024 * 1024,
        GM_API_KEY='',
        PROXY=False,
        PROFILE=False,
       # SESSION_COOKIE_SECURE=True,
        HOME_URL='/',
        SESSION_COOKIE_HTTPONLY=True,
        SESSION_COOKIE_SAMESITE='Lax',
        SITE_NAME='muziekladder.nl',
        SERVER_NAME='muziekladder.nl',
        APPLICATION_ROOT='/',
        PREFERRED_URL_SCHEME='https',
        DEBUG=True,
        SECRET_KEY='dev123',
        MYSQL_USER='db_user',
        MYSQL_PASSWORD='password',
        MYSQL_DB='muziekladder_db',
        MYSQL_HOST='mariadb',
        RESULTS_PER_PAGE=200,
        POSTS_PER_PAGE=20,
        SEARCH_RESULTS_PER_PAGE=100,
        SOLR='http://solr:8983/solr',
        SOLRCORE='mzklddr1',
        SOLR_API= 'http://solr:8983/api',
        SOLR_PARAMS_DEFAULT={
                        "_q0":{'q.op':'AND'},
                        "_q1":{'defType':'edismax'},
                        "_q2":{'qf':'date_start_dt event_title_t event_description_t event_description_txt_nl event_description_txt_en venue_title_s venue_description_t zip_s address_s city_s country_s _text_',},
                        "place":{'json.facet':'{place:{type:terms,field:city_s,sort:"count desc",limit:100}}'},
                        "country":{'json.facet':'{country:{type:terms,field:country_s,sort:"count desc",limit:100}}'},
                        "city_group":{'json.facet':'{city_group:{type:terms,field:city_groupings_id_ss,sort:"count desc",limit:100}}'},
                        "venue":{'json.facet':'{venue:{type:terms,field:venue_title_s,sort:"count desc",limit:100}}'},
        },
        SOLR_PARAMS_ALT={},
        SOLR_ALLOWED_USER_PARAMS=['q','fq','start','sort'], # params that are allowed to go directly from flask request args to solr server
        BABEL_DEFAULT_LOCALE='nl',
        LANGUAGES=['nl','en'],
        IMAGE_DIR='/tmp/images',
        IMAGE_FORMATS = {   "thumb":(60,60),
                            "list":(400,400),
                            "detail":(500,500),
                            "big":(650,650),
                            "original":(0,0),},
        BASE_ROUTES = {
            "agenda":"muziek",
            "muziekformulier":"muziekformulier",
            "locaties" : "locaties",
            "uitgaan" : "uitgaan",
        },
        EVENT_CACHE = os.path.join(app.instance_path, 'event_cache'),
        USER_DATA_DIR = os.path.join(app.instance_path, 'user_data'),
        UPLOAD_FOLDER = os.path.join(app.instance_path, 'uploads'),
        UPLOAD_ARCHIVE =  os.path.join(app.instance_path, 'upload_archive'),
        ALLOWED_MIMES = {"image/bmp", "image/jpeg", "image/x-png", "image/png", "image/gif"},
        #        USER_DB = os.path.join(app.instance_path,'user_db'),
        SMTP_SERVER='SMTP.SERVER.com',
        SMTP_PORT='465',
        FROM_MAIL='info@domainname.com',
        ADMIN_MAIL='info@domainname.com',
        SMTP_USER='info@domainname.com',
        SMTP_PASS='secretvery',
        MAIL_WAIT_TIME=60, # (seconds) prevent hammering of mail-servers
        LOGIN_BLOCK_DURATION=36000, #10minutes
        LOGIN_VALID_DURATION=36000, #10minutes
        CAPTCHA_ENABLE=True,
        CAPTCHA_LENGTH=7,
        CAPTCHA_WIDTH=300,
        CAPTCHA_HEIGHT=100,
        CAPTCHA_INCLUDE_ALPHABET=False,
        SESSION_TYPE = 'cachelib',
        SESSION_SERIALIZATION_FORMAT = 'json',
        SESSION_CACHELIB = FileSystemCache(threshold=500, cache_dir="/sessions"),
        CACHE_TYPE="FileSystemCache",  # Flask-Caching related configs
        #CACHE_TYPE="NullCache",  # disable cache
        CACHE_DEFAULT_TIMEOUT=36000,
        CACHE_IGNORE_ERRORS=False,
        CACHE_DIR=os.path.join(app.instance_path, 'app_cache'),
        CACHE_THRESHOLD=1000,
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_port=1)

    # server side session
    Session(app)

    # session captcha
    captcha.init_app(app)

    cache.init_app(app)

    # init db
    db.init_app(app)

    # flask-login
    login_manager = LoginManager()

    @login_manager.user_loader
    def load_user(user_id):
        user = User(user_id)
        logging.debug(f"{__file__}: loading user {user_id}")
        if user.user_id:
            return user
        elif user.is_authenticated:
            logging.debug("{__file__}: authenticated user {user_id} not found")
            session.clear()
            return None

    login_manager.init_app(app)
    login_manager.login_view = 'user.login'

    # BOF babel
    babel = Babel(app)

    def get_locale():
        locale = g.get('lang_code', app.config['BABEL_DEFAULT_LOCALE'])
        if locale not in app.config['LANGUAGES']:
            locale = app.config['BABEL_DEFAULT_LOCALE'] 
        return locale

    babel.init_app(app, locale_selector=get_locale)
    app.jinja_env.globals['get_locale'] = get_locale


    @app.url_defaults
    def add_language_code(endpoint, values):
        if 'lang_code' in values:
            return
        if app.url_map.is_endpoint_expecting(endpoint, 'lang_code'):
            values['lang_code'] = g.get('lang_code',app.config['BABEL_DEFAULT_LOCALE'])
            if values['lang_code'] not in app.config['LANGUAGES']:
                values['lang_code'] = app.config['BABEL_DEFAULT_LOCALE']

    @app.before_request
    def ttime():
        if app.config['PROFILE']:
            g.timer=time.time()

    @app.after_request
    def stopTimer(response):
        if app.config['PROFILE']:
            timer2 = time.time() - g.timer
            logging.debug('[###] ' + str(timer2))
        return response

    @app.before_request
    def ensure_lang_support():
        lang_code = g.get('lang_code', None)
        if lang_code and lang_code not in app.config['LANGUAGES']:
            return abort(404)


    @app.url_value_preprocessor
    def get_lang_code(endpoint, values):
        if values is not None:
            g.lang_code = values.pop('lang_code',app.config['BABEL_DEFAULT_LOCALE'])


    # EOF babel
    # jinja preprocessors:
    @app.before_request
    def set_darkmode():
        app.jinja_env.globals['darkmode'] = request.cookies.get('darkmode')


    @app.before_request
    def set_js_globals():
        app.jinja_env.globals['home_url'] = app.config['HOME_URL'] #request.root_url
        app.jinja_env.globals['server_name'] = app.config['SERVER_NAME']
        app.jinja_env.globals['preferred_url_scheme'] = app.config['PREFERRED_URL_SCHEME']


    def page_not_found(e):
        return render_template('404.html'), 404


    app.register_error_handler(404, page_not_found)


    @app.context_processor
    def top_menu(): 
        m = [
            {'url':'agenda.list', 'text':_('Calendar')},
            {'url':'uitgaan.city', 'text':_('Locations')},
            {'url':'tips.listing', 'text':_('Tips')},
        ]
        return dict(menu=m)


    @app.context_processor
    def utility_processor():
        def city_group_name(group_id,locale):
            grouping = collect.get_city_grouping(group_id)
            if locale in grouping['group_translations']:
                return grouping['group_translations'][locale]
            else:
                return grouping['group']['group_name']


        def t_format_date(date, format_string):
            return format_datetime(date, format_string)


        def _format_date_string(value, from_format, to_format):
            d = datetime.strptime(value.strip(), from_format)
            rv = format_datetime(d, to_format)
            return rv


        return  dict(   can_edit=collect.can_edit,
                        city_group_name=city_group_name,
                        format_datetime=t_format_date,
                        format_date_string=_format_date_string,
                        gig_link=collect.gig_link,
                        post_link=collect.post_link,
                        venue_link=collect.venue_link,
                        city_link=collect.city_link,
                        city_agenda_link=collect.city_agenda_link,
                        img_hash = collect.img_url_to_hash,
                        country_name = collect.country_name)


    @app.template_filter()
    def format_solr_date(value, format='agenda_list'):
        if format == 'agenda_list':
            d = datetime.strptime(value,'%Y-%m-%dT00:00:00Z')
            rv = format_datetime(d,'EEE dd MMM')
        elif format == 'agenda_list_header':
            d = datetime.strptime(value,'%Y-%m-%dT00:00:00Z')
            rv = format_datetime(d,'EEEE dd MMMM')
        elif format == 'agenda_header_date':
            rv = format_datetime(value,'EEEE dd MMMM')
        elif format == 'search_results':
            d = datetime.strptime(value,'%Y-%m-%dT00:00:00Z')
            rv = format_datetime(d,'d MMMM YYYY')
        return rv


    # blueprints
    BS = app.config['BASE_ROUTES']
    app.register_blueprint(home.bp,url_prefix='/', name='home_default')
    app.register_blueprint(home.bp,url_prefix='/<lang_code>/')

    app.register_blueprint(agenda.bp,url_prefix='/' + BS['agenda'],name='agenda_default')
    app.register_blueprint(agenda.bp,url_prefix='/<lang_code>/' + BS['agenda'])

    app.register_blueprint(muziekformulier.bp,url_prefix='/' + BS['muziekformulier'],name='muziekformulier_default')
    app.register_blueprint(muziekformulier.bp,url_prefix='/<lang_code>/' + BS['muziekformulier'])

    app.register_blueprint(gig.bp,url_prefix='/gig',name='gig_default')
    app.register_blueprint(gig.bp,url_prefix='/<lang_code>/gig')

    app.register_blueprint(locaties.bp,url_prefix='/' + BS['locaties'], name='locaties_default')
    app.register_blueprint(locaties.bp,url_prefix='/<lang_code>/' + BS['locaties'])

    app.register_blueprint(uitgaan.bp,url_prefix='/' + BS['uitgaan'],name='uitgaan_default')
    app.register_blueprint(uitgaan.bp,url_prefix='/<lang_code>/'+BS['uitgaan'])

    app.register_blueprint(image.bp,url_prefix='/image',name='image_default')
    app.register_blueprint(image.bp,url_prefix='/<lang_code>/image')

    app.register_blueprint(search.bp,url_prefix='/search',name='search_default')
    app.register_blueprint(search.bp,url_prefix='/<lang_code>/search')

    app.register_blueprint(user.bp,url_prefix='/user',name='user_default')
    app.register_blueprint(user.bp,url_prefix='/<lang_code>/user')

    app.register_blueprint(tips.bp,url_prefix='/tips',name='tips_default')
    app.register_blueprint(tips.bp,url_prefix='/<lang_code>/tips')

    app.register_blueprint(content.bp,url_prefix='/content',name='content_default')
    app.register_blueprint(content.bp,url_prefix='/<lang_code>/content')

    app.register_blueprint(contact.bp,url_prefix='/contact',name='contact_default')
    app.register_blueprint(contact.bp,url_prefix='/<lang_code>/contact')

    app.register_blueprint(cmd.bp,url_prefix='/cmd',name='cmd')


    @app.route('/favicon.ico')
    @app.route('/favicon.ico/')
    def favicon():
        return send_from_directory(os.path.join(app.root_path, 'static'),
            'favicon.ico', mimetype='image/vnd.microsoft.icon')


    @app.route('/ads.txt')
    @app.route('/robots.txt')
    def static_from_root():
        return send_from_directory(app.static_folder, request.path[1:])


    @app.route('/hello')
    def hello():
        return 'Hello !'


    @click.command()
    @with_appcontext
    def clear_cache():
        print (app.config['CACHE_DEFAULT_TIMEOUT'])
        cache.clear()

    app.cli.add_command(clear_cache)

    return app
