from flask import Blueprint,render_template,g,abort,request,current_app,url_for
from flask_babel import format_datetime,_
import datetime
from mzklddr.helpers import sql
from mzklddr.helpers import collect
from mzklddr.helpers import util as u
from mzklddr.helpers.caching import cache
import re


bp=Blueprint('agenda', __name__)


@bp.route('/')
@bp.route('/<location_or_day>')
@bp.route('/<location_or_day>/')
@bp.route('/<location>/<day>')
@cache.cached(key_prefix=u.make_key)
def list(location_or_day=None, location=None, day=None):
    logger = current_app.logger
    data = {}
    day_filter=None
    group_filter=None
    city_filter=None
    relevant_cities=[]
    max_city_distance=20

    try:
        page = int(request.args.get('pagina',0))
    except Exception as e:
        logger.exception(e)
        abort(404)

    if location_or_day:
        if 'agenda-' in location_or_day:
            day_filter = location_or_day
        else:
            location = location_or_day
    elif day:
        day_filter = day

    if day_filter:
        try:
            day_filter = re.split(r'[\.-]', day_filter)[1]
            day_filter = int(day_filter)
        except Exception as e:
            logger.exception(e)
            abort(404)

    if location:
        if 'regio-' in location:
            group_id = location.split('regio-')[1]
            response =  sql.get_city_groupings(group_id)
            if len(response):
                group_filter = group_id
                data['grouping'] = response[0]
        elif re.search(r'^[0-9]+-', location):
            city_filter = location.split('-')[0]
            city_filter = int(city_filter)

    if city_filter:
        response = sql.get_city(city_filter)
        if response:
            city = response
            data['city'] = city
            relevant_cities = collect.get_nearby_cities(city_row=city,
               max_distance=15)
            max_city_distance=15
        else:
            abort(404)

    event_list = events(day_filter, relevant_cities, group_filter, page)

    if not len(event_list[1]) and city_filter:
            """ second attempt: 
                (if city_filter is the bottleneck:
                 search larger radius)"""
            max_city_distance=35
            relevant_cities = collect.get_nearby_cities(city_row=city,
                max_distance=max_city_distance)
            event_list = events(day_filter, relevant_cities, None, page)
            if not len(event_list[1]) and city_filter:
                """ third and last attempt """
                max_city_distance=65
                relevant_cities = collect.get_nearby_cities(city_row=city,
                    max_distance=max_city_distance)
                event_list = events(day_filter, relevant_cities, None, page)

    data['events'] = u.dedup_events(event_list[1])
    data['max_city_distance'] = max_city_distance
    data['number_of_results'] = len(event_list[1])
    data['start_date'] = format_datetime(event_list[0],'EEEE d MMMM yyyy')
    data['cities'] = sql.get_cities(group_id=group_filter)
    data['groupings'] = sql.get_city_groupings()
    data['date_options'] = get_date_selector_dates()
    data['day_filter'] = day_filter
    data['group_filter'] = group_filter
    data['city_filter'] = city_filter
    data['page'] = page
    data['res_per_pag'] = current_app.config['RESULTS_PER_PAGE'] 
    crumbs = u.crumb(url_for('agenda.list'), _('Calendar'))
 
    return render_template('agenda.html',data=data, breadcrumbs=crumbs)


def events(day_filter, relevant_cities, group_filter, page):
    logger = current_app.logger
    if day_filter:
        day = datetime.date.today() + datetime.timedelta(days=day_filter)
    else:
        day = datetime.date.today()

    start_date = day.strftime('%Y-%m-%d')
    start = page * current_app.config['RESULTS_PER_PAGE']
    events = []
    if not relevant_cities:
        events = sql.get_events(date_start=start_date,
                                    group_id=group_filter,
                                    start=start)
    else:
        events = sql.get_events(date_start=start_date,
                                    multi_city_ids=relevant_cities,
                                    group_id=group_filter,
                                    start=start)
    return [day, events]


def get_date_selector_dates():
    dates = []
    start = datetime.date.today()
    for day in range(90):
        option = start + datetime.timedelta(days=day)
        dates.append(format_datetime(option,'dd MMM yyyy -- EEEE'))
    return dates
