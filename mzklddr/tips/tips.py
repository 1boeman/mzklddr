from flask import Blueprint, render_template, abort, url_for, redirect, current_app
import logging
from mzklddr.models.post import Post
from flask_babel import _
from mzklddr.helpers import sql
from mzklddr.helpers import util as u
from mzklddr.helpers import collect
from mzklddr.helpers.caching import cache

bp = Blueprint('tips', __name__)


@bp.route('/<post_id_slug>')
@cache.cached()
def tip(post_id_slug):
    _list = post_id_slug.split("_")
    post_id = _list[0]
    post = Post(post_id)
    if post.post_id:
        # check slug and redirect if incorrect 
        canonical_slug = post.detail_link
        if post_id_slug != canonical_slug:
            return redirect(url_for('tips.tip', post_id_slug=canonical_slug))
        breadcrumbs = u.crumb(url_for('tips.listing', page=0), _('Tips'), [])
        u.crumb(url_for('tips.tip', post_id_slug=post_id_slug), post.post_title, breadcrumbs)

        return render_template('post.html', post=post, breadcrumbs=breadcrumbs)
    else:
        abort(404)


@bp.route('/list')
@bp.route('/list/')
@bp.route('/list/<page>')
@cache.cached()
def listing(page=0):
    try:
        page = int(page)
    except ValueError:
        abort(404)

    crumbs = u.crumb(url_for('tips.listing', page=0), _('Tips'), [])
    data = collect.post_listing(page=page) 
    if "total_pages" in data and page >= data['total_pages']:
        abort(404)
    return render_template('post-list.html', data=data, breadcrumbs = crumbs)


@bp.route('/lastids')
def get_last_id():
   return sql.get_last_post_id(current_app.config['POSTS_PER_PAGE'])


@bp.route('/list_latest/')
@bp.route('/list_latest/<is_home>')
def latest(is_home=False):
    data={}
    posts = collect.post_list()
    data['posts'] = posts
    if is_home:
        return render_template('posts_include.html', data=data, home_tips=1)
    else:
        return render_template('posts_include.html', data=data, main_tips=1)
