FROM python:3.12-alpine3.21
RUN apk update && \
    apk upgrade

# Create app directory
WORKDIR /app

# Install app dependencies
COPY requirements.txt ./

RUN apk add --no-cache build-base gcc

RUN pip install --no-cache-dir -r requirements.txt

# Bundle app source
COPY . .

ENV FLASK_APP=mzklddr
ENV FLASK_ENV=production

#EXPOSE 5000
EXPOSE 8000

#CMD [ "flask", "run","--host","0.0.0.0","--port","5000","--debug","--extra-files","/app/mzklddr/translations/nl/LC_MESSAGES/messages.mo"]
CMD ["gunicorn", "-w 4", "-b 0.0.0.0","mzklddr:create_app()"]
